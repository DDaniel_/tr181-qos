/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "tr181-qos.h"
#include "dm_tr181-qos.h"
#include "qos-controller.h"
#include <qosnode/qos-node-api.h>
#include <netmodel/client.h>

#include <debug/sahtrace.h>

#define ME "main"
typedef struct _app_t {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
} qos_app_t;

static qos_app_t qos;

void _qos_app_start(UNUSED const char* const sig_name,
                    UNUSED const amxc_var_t* const data,
                    UNUSED void* const priv) {
    qos_node_api_init(qos.dm);
    netmodel_initialize();
    if(qos_controller_init()) {
        SAH_TRACEZ_ERROR(ME, "Failed to initialise controller");
        amxo_parser_rinvoke_entry_points(qos.parser, qos.dm, AMXO_STOP);
    }
}

amxd_dm_t* qos_get_dm(void) {
    return qos.dm;
}

amxo_parser_t* qos_get_parser(void) {
    return qos.parser;
}

amxc_var_t* qos_get_config(void) {
    return &qos.parser->config;
}

amxd_object_t* qos_get_object(const char* path) {
    amxd_object_t* object = NULL;

    when_str_empty(path, exit);
    object = amxd_dm_findf(qos_get_dm(), "%s", path);

exit:
    return object;
}

//! [entry point]
int _qos_main(int reason,
              amxd_dm_t* dm,
              amxo_parser_t* parser) {

    int retval = 0;

    switch(reason) {
    case 0:     // START
        SAH_TRACEZ_WARNING(ME, "Starting TR-181 QoS Manager");
        qos.dm = dm;
        qos.parser = parser;
        break;
    case 1:     // STOP
        SAH_TRACEZ_WARNING(ME, "Stop TR-181 QoS Manager");
        retval = qos_controller_deinit();
        netmodel_cleanup();
        qos.dm = NULL;
        qos.parser = NULL;
        break;
    }

    retval = (retval != 0);

    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Exit with error");
    }

    return retval;
}
//! [entry point]

