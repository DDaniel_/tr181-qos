/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <debug/sahtrace.h>
#include "qos.h"
#include <amxc/amxc_variant.h>
#include <amxm/amxm.h>
#include <qos-module-mngr.h>
#define ME "mod_mngr"
#define MOD_QUEUE_DM_MNGR "dm-mngr"

static int dm_mngr_load_controllers(void) {
    int retval = 0;
    amxd_dm_t* dm = qos_get_dm();
    amxd_object_t* instance = amxd_dm_findf(dm, QOS_TR181_DEVICE_QOS_PATH);
    const amxc_var_t* controllers = amxd_object_get_param_value(instance, "SupportedControllers");

    amxc_var_t lcontrollers;
    amxm_shared_object_t* so = NULL;
    amxc_string_t mod_path;

    amxc_var_init(&lcontrollers);
    amxc_string_init(&mod_path, 0);

    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        amxc_string_setf(&mod_path, "/usr/lib/amx/tr181-qos/modules/%s.so", name);
        SAH_TRACEZ_INFO(ME, "Loading controller '%s' (file = %s)", name, amxc_string_get(&mod_path, 0));
        retval = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        SAH_TRACEZ_INFO(ME, "Loading controller '%s' returned %d", name, retval);
        if(retval != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to load controller '%s'", name);
        }
        when_failed(retval, exit);
    }


exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lcontrollers);
    return 0;
}

int qos_module_mngr_init(void) {
    int retval = -1;
    amxm_shared_object_t* so = amxm_get_so("self");
    amxm_module_t* mod = NULL;
    when_null(so, exit);

    when_failed(amxm_module_register(&mod, so, MOD_QUEUE_DM_MNGR), exit);
    SAH_TRACEZ_INFO(ME, "Data model manager init");

    retval = dm_mngr_load_controllers();

exit:
    return retval;
}

static amxc_var_t* qos_module_build_variant(amxd_object_t* object) {
    amxc_var_t* var = NULL;
    amxc_var_t* params = NULL;

    when_null(object, exit);
    when_failed(amxc_var_new(&var), exit);

    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, var, "index", amxd_object_get_index(object));
    params = amxc_var_add_key(amxc_htable_t, var, "parameters", NULL);
    amxd_object_get_params(object, params, amxd_dm_access_protected);

exit:
    return var;
}

amxc_var_t* qos_module_build_named_variant(const char* name, amxd_object_t* object) {
    amxc_var_t* data = NULL;
    amxc_var_t* var = NULL;

    when_str_empty(name, exit);
    when_null(object, exit);
    when_failed(amxc_var_new(&data), exit);

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);

    var = qos_module_build_variant(object);
    amxc_var_add_key(amxc_htable_t, data, name, amxc_var_constcast(amxc_htable_t, var));
    amxc_var_delete(&var);

exit:
    return data;
}

amxc_var_t* qos_module_build_named_variant_append(const char* name, amxd_object_t* object, amxc_var_t* data) {
    amxc_var_t* var = NULL;

    when_str_empty(name, exit);
    when_null(object, exit);
    when_null(data, exit);

    var = qos_module_build_variant(object);
    amxc_var_add_key(amxc_htable_t, data, name, amxc_var_constcast(amxc_htable_t, var));
    amxc_var_delete(&var);

exit:
    return data;
}

