/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "qos-assert.h"
#include "dm_tr181-qos.h"

#include "test_qos.h"
#include "tr181-qos.h"
#include "qos.h"
#include <qoscommon/qos-queue.h>
#include "qos-controller.h"

#define MOD_DM_MNGR "dm-mngr"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "qos_test.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static amxd_object_t* get_qos_object() {
    amxd_object_t* qos_object = NULL;

    qos_object = amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_PATH);
    assert_non_null(qos_object);

    return qos_object;
}

int test_qos_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "qos_main",
                                            AMXO_FUNC(_qos_main)), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _qos_main(0, &dm, &parser);
    _qos_app_start(NULL, NULL, NULL);
    handle_events();

    return 0;
}

int test_qos_teardown(UNUSED void** state) {
    _qos_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_qos_new_delete(UNUSED void** state) {
    int retval = -1;
    qos_t* qos = NULL;
    amxd_object_t qos_instance = {0};

    //first deregister the module
    amxm_shared_object_t* so = amxm_get_so("self");
    amxm_module_t* mod = amxm_so_get_module(so, MOD_DM_MNGR);
    amxm_module_deregister(&mod);

    retval = qos_new(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_new(&qos, NULL);
    assert_int_equal(retval, -1);

    retval = qos_new(NULL, &qos_instance);
    assert_int_equal(retval, -1);

    retval = qos_new(&qos, &qos_instance);
    assert_int_equal(retval, 0);
    assert_non_null(qos);
    assert_ptr_equal(qos, qos_instance.priv);

    retval = qos_delete(NULL);
    assert_int_equal(retval, -1);

    retval = qos_delete(&qos);
    assert_int_equal(retval, 0);
    assert_null(qos);

    retval = qos_delete(&qos);
    assert_int_equal(retval, -1);
    assert_null(qos);
}

void test_qos_get_dm_object_priv_data(UNUSED void** state) {
    amxd_object_t* qos_object = NULL;
    qos_t* qos = NULL;

    qos_object = get_qos_object();

    qos = qos_get_dm_object_priv_data(NULL);
    assert_null(qos);

    qos = qos_get_dm_object_priv_data(qos_object);
    assert_non_null(qos);
    assert_ptr_equal(qos->dm_object, qos_object);
}

void test_qos_get_dm_object(UNUSED void** state) {
    amxd_object_t* qos_object = NULL;
    amxd_object_t* dm_object = NULL;
    qos_t* qos = NULL;

    qos_object = get_qos_object();
    assert_non_null(qos_object->priv);

    qos = qos_get_dm_object_priv_data(qos_object);
    assert_non_null(qos);

    dm_object = qos_get_dm_object(NULL);
    assert_null(dm_object);

    dm_object = qos_get_dm_object(qos);
    assert_non_null(dm_object);
    assert_ptr_equal(qos_object, dm_object);
}

void test_qos_dm_get_max_classification_entries(UNUSED void** state) {
    amxd_object_t* qos_object = NULL;
    qos_t* qos = NULL;
    uint32_t max_classification_entries = 0;

    qos_object = get_qos_object();

    qos = qos_get_dm_object_priv_data(qos_object);
    assert_non_null(qos);

    max_classification_entries = qos_dm_get_max_classification_entries(NULL);
    assert_int_equal(max_classification_entries, 0);

    max_classification_entries = qos_dm_get_max_classification_entries(qos);
    assert_int_equal(max_classification_entries, 12);
}

void test_qos_dm_get_max_queue_entries(UNUSED void** state) {
    amxd_object_t* qos_object = NULL;
    qos_t* qos = NULL;
    uint32_t max_queue_entries = 0;

    qos_object = get_qos_object();

    qos = qos_get_dm_object_priv_data(qos_object);
    assert_non_null(qos);

    max_queue_entries = qos_dm_get_max_queue_entries(NULL);
    assert_int_equal(max_queue_entries, 0);

    max_queue_entries = qos_dm_get_max_queue_entries(qos);
    assert_int_equal(max_queue_entries, 8);

}

