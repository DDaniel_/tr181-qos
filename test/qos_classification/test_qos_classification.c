/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include "qos-assert.h"
#include "dm_tr181-qos.h"
#include "dm-qos-classification.h"

#include "test_qos_classification.h"
#include "tr181-qos.h"
#include "qos.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "qos_classification_test.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static amxd_object_t* get_classification_instance(uint32_t index) {
    amxd_object_t* classification_object = NULL;
    amxd_object_t* classification_instance = NULL;

    classification_object = amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_CLASSIFICATION_PATH);
    assert_non_null(classification_object);

    classification_instance = amxd_object_get_instance(classification_object, NULL, index);
    assert_non_null(classification_instance);

    return classification_instance;
}

static amxd_object_t* get_queue_instance(uint32_t index) {
    amxd_object_t* queue_object = NULL;
    amxd_object_t* queue_instance = NULL;

    queue_object = amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_QUEUE_PATH);
    assert_non_null(queue_object);

    queue_instance = amxd_object_get_instance(queue_object, NULL, index);
    assert_non_null(queue_instance);

    return queue_instance;
}

int test_qos_classification_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "qos_main",
                                            AMXO_FUNC(_qos_main)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_classification_evt_instance_added",
                                            AMXO_FUNC(_dm_qos_classification_evt_instance_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_classification_evt_instance_changed",
                                            AMXO_FUNC(_dm_qos_classification_evt_instance_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_classification_action_on_destroy",
                                            AMXO_FUNC(_dm_qos_classification_action_on_destroy)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _qos_main(0, &dm, &parser);
    _qos_app_start(NULL, NULL, NULL);
    handle_events();

    return 0;
}

int test_qos_classification_teardown(UNUSED void** state) {
    _qos_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}


void test_dm_qos_classification_evt_instance_changed(UNUSED void** state) {
    uint32_t index = 1;
    amxc_var_t params;
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;
    bool enable = false;

    classification_instance = get_classification_instance(index);
    assert_non_null(classification_instance);
    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);

    enable = qos_classification_dm_get_enable(classification);
    assert_true(enable);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &params, "Enable", false);
    amxd_object_send_changed(classification_instance, &params, false);
    amxc_var_clean(&params);

    handle_events();

}

void test_dm_qos_classification_evt_instance_added(UNUSED void** state) {
    amxd_object_t* classification_object = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;

    classification_object = amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_CLASSIFICATION_PATH);
    assert_non_null(classification_object);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);


    assert_int_equal(amxd_object_invoke_function(classification_object, "_add", &args, &ret), amxd_status_ok);
    assert_false(amxc_var_is_null(&ret));

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_dm_qos_classification_action_on_destroy(UNUSED void** state) {
    uint32_t index = 1;
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;

    classification_instance = get_classification_instance(index);
    assert_non_null(classification_instance);
    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);

    amxd_object_delete(&classification_instance);
    assert_null(classification_instance);
}

void test_qos_classification_new_delete(UNUSED void** state) {
    int retval = -1;
    qos_classification_t* classification = NULL;
    amxd_object_t classification_instance = {0};

    retval = qos_classification_new(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_classification_new(&classification, NULL);
    assert_int_equal(retval, -1);

    retval = qos_classification_new(NULL, &classification_instance);
    assert_int_equal(retval, -1);

    retval = qos_classification_new(&classification, &classification_instance);
    assert_int_equal(retval, 0);
    assert_non_null(classification);
    assert_ptr_equal(classification, classification_instance.priv);

    retval = qos_classification_delete(NULL);
    assert_int_equal(retval, -1);

    retval = qos_classification_delete(&classification);
    assert_int_equal(retval, 0);
    assert_null(classification);

    retval = qos_classification_delete(&classification);
    assert_int_equal(retval, -1);
    assert_null(classification);
}

void test_qos_classification_init_deinit(UNUSED void** state) {
    int retval = -1;
    qos_classification_t classification;
    amxd_object_t classification_instance = {0};

    retval = qos_classification_init(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_classification_init(&classification, NULL);
    assert_int_equal(retval, -1);

    retval = qos_classification_init(&classification, &classification_instance);
    assert_int_equal(retval, 0);

    assert_non_null(classification.folder4);
    assert_non_null(classification.folder6);

    retval = qos_classification_deinit(NULL);
    assert_int_equal(retval, -1);

    retval = qos_classification_deinit(&classification);
    assert_int_equal(retval, 0);

    assert_null(classification.dm_object->priv);
    assert_null(classification.folder4);
    assert_null(classification.folder6);
}

void test_qos_classification_get_dm_object(UNUSED void** state) {
    uint32_t index = 1;
    amxd_object_t* classification_instance = NULL;
    amxd_object_t* dm_object = NULL;
    qos_classification_t* classification = NULL;

    classification_instance = get_classification_instance(index);
    assert_non_null(classification_instance->priv);

    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);

    dm_object = qos_classification_get_dm_object(NULL);
    assert_null(dm_object);

    dm_object = qos_classification_get_dm_object(classification);
    assert_non_null(dm_object);
    assert_ptr_equal(dm_object, classification_instance);
}

void test_qos_classification_activate_deactivate(UNUSED void** state) {
    int retval = -1;
    uint32_t index = 2;
    qos_status_t status;
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;

    classification_instance = get_classification_instance(index);
    assert_non_null(classification_instance->priv);

    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);

    retval = qos_classification_activate(classification);
    assert_int_equal(retval, 0);

    status = qos_classification_dm_get_status(classification);
    assert_int_equal(status, QOS_STATUS_ENABLED);

    retval = qos_classification_deactivate(classification);
    assert_int_equal(retval, 0);

    status = qos_classification_dm_get_status(classification);
    assert_int_equal(status, QOS_STATUS_DISABLED);
}

void test_qos_classification_dm_get_enable(UNUSED void** state) {
    uint32_t index = 1;
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;
    bool enable = true;

    classification_instance = get_classification_instance(index);
    assert_non_null(classification_instance->priv);

    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);

    enable = qos_classification_dm_get_enable(NULL);
    assert_false(enable);

    enable = qos_classification_dm_get_enable(classification);
    assert_true(enable);

}

void test_qos_classification_dm_set_enable(UNUSED void** state) {
    int retval = -1;
    uint32_t index = 1;
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;

    classification_instance = get_classification_instance(index);
    assert_non_null(classification_instance->priv);

    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);

    retval = qos_classification_dm_set_enable(NULL, false);
    assert_int_equal(retval, -1);

    retval = qos_classification_dm_set_enable(NULL, true);
    assert_int_equal(retval, -1);

    retval = qos_classification_dm_set_enable(classification, false);
    assert_int_equal(retval, 0);
    assert_false(qos_classification_dm_get_enable(classification));

    retval = qos_classification_dm_set_enable(classification, true);
    assert_int_equal(retval, 0);
    assert_true(qos_classification_dm_get_enable(classification));
}

void test_qos_classification_dm_get_set_status(UNUSED void** state) {
    int retval = -1;
    uint32_t index = 1;
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;
    qos_status_t status = QOS_STATUS_DISABLED;
    qos_status_t valid_status = QOS_STATUS_DISABLED;

    classification_instance = get_classification_instance(index);
    assert_non_null(classification_instance->priv);

    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);

    status = qos_classification_dm_get_status(NULL);
    assert_int_equal(status, QOS_STATUS_DISABLED);

    status = qos_classification_dm_get_status(classification);
    assert_int_equal(status, QOS_STATUS_ENABLED);

    retval = qos_classification_dm_set_status(classification, QOS_STATUS_LAST);
    status = qos_classification_dm_get_status(classification);
    assert_int_equal(retval, -1);
    assert_int_equal(status, QOS_STATUS_ENABLED);

    while(valid_status < QOS_STATUS_LAST) {
        retval = qos_classification_dm_set_status(classification, valid_status);
        status = qos_classification_dm_get_status(classification);
        assert_int_equal(retval, 0);
        assert_int_equal(status, valid_status);

        valid_status++;
    }
}

void test_qos_classification_dm_get_set_order(UNUSED void** state) {
    int retval = -1;
    uint32_t index = 1;
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;
    uint32_t order;
    uint32_t order_first = 3;
    uint32_t order_second = 7;

    classification_instance = get_classification_instance(index);
    assert_non_null(classification_instance->priv);

    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);

    order = qos_classification_dm_get_order(classification);
    assert_int_equal(order, order_first);

    retval = qos_classification_dm_set_order(classification, order_second);
    order = qos_classification_dm_get_order(classification);
    assert_int_equal(retval, 0);
    assert_int_equal(order, order_second);
}

void test_qos_classification_dm_get_alias(UNUSED void** state) {
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;
    const char* alias = NULL;

    alias = qos_classification_dm_get_alias(NULL);
    assert_null(alias);

    classification_instance = get_classification_instance(1);
    assert_non_null(classification_instance->priv);
    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);
    alias = qos_classification_dm_get_alias(classification);
    assert_non_null(alias);
    assert_string_equal(alias, "test_classification_default");
}

void test_qos_classification_dm_get_dest_ip(UNUSED void** state) {
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;
    const char* dest_ip = NULL;

    dest_ip = qos_classification_dm_get_dest_ip(NULL);
    assert_null(dest_ip);

    classification_instance = get_classification_instance(1);
    assert_non_null(classification_instance->priv);
    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);
    dest_ip = qos_classification_dm_get_dest_ip(classification);
    assert_non_null(dest_ip);
    assert_string_equal(dest_ip, "192.168.0.2");
}

void test_qos_classification_dm_get_dest_mask(UNUSED void** state) {
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;
    const char* destmask = NULL;

    destmask = qos_classification_dm_get_dest_mask(NULL);
    assert_null(destmask);

    classification_instance = get_classification_instance(1);
    assert_non_null(classification_instance->priv);
    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);
    destmask = qos_classification_dm_get_dest_mask(classification);
    assert_non_null(destmask);
    assert_string_equal(destmask, "255.255.255.0");
}

void test_qos_classification_dm_get_dest_ip_exclude(UNUSED void** state) {
    uint32_t index = 1;
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;
    bool dest_ip_exclude = false;

    classification_instance = get_classification_instance(index);
    assert_non_null(classification_instance->priv);

    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);

    dest_ip_exclude = qos_classification_dm_get_dest_ip_exclude(NULL);
    assert_false(dest_ip_exclude);

    dest_ip_exclude = qos_classification_dm_get_dest_ip_exclude(classification);
    assert_true(dest_ip_exclude);
}

void test_qos_classification_dm_set_dest_ip_exclude(UNUSED void** state) {
    int retval = -1;
    uint32_t index = 1;
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;

    classification_instance = get_classification_instance(index);
    assert_non_null(classification_instance->priv);

    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);

    retval = qos_classification_dm_set_dest_ip_exclude(NULL, false);
    assert_int_equal(retval, -1);

    retval = qos_classification_dm_set_dest_ip_exclude(NULL, true);
    assert_int_equal(retval, -1);

    retval = qos_classification_dm_set_dest_ip_exclude(classification, false);
    assert_int_equal(retval, 0);
    assert_false(qos_classification_dm_get_dest_ip_exclude(classification));

    retval = qos_classification_dm_set_dest_ip_exclude(classification, true);
    assert_int_equal(retval, 0);
    assert_true(qos_classification_dm_get_dest_ip_exclude(classification));

}

void test_qos_classification_dm_get_source_ip(UNUSED void** state) {
    uint32_t index = 1;
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;
    const char* source_ip = NULL;

    classification_instance = get_classification_instance(index);
    assert_non_null(classification_instance->priv);

    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);

    source_ip = qos_classification_dm_get_source_ip(NULL);
    assert_null(source_ip);

    source_ip = qos_classification_dm_get_source_ip(classification);
    assert_string_equal(source_ip, "192.168.3.1");

}

void test_qos_classification_dm_get_source_mask(UNUSED void** state) {
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;
    const char* sourcemask = NULL;

    sourcemask = qos_classification_dm_get_source_mask(NULL);
    assert_null(sourcemask);

    classification_instance = get_classification_instance(1);
    assert_non_null(classification_instance->priv);
    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);
    sourcemask = qos_classification_dm_get_source_mask(classification);
    assert_non_null(sourcemask);
    assert_string_equal(sourcemask, "255.255.255.0");
}

void test_qos_classification_dm_get_source_ip_exclude(UNUSED void** state) {
    uint32_t index = 1;
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;
    bool source_ip_exclude = false;

    classification_instance = get_classification_instance(index);
    assert_non_null(classification_instance->priv);

    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);

    source_ip_exclude = qos_classification_dm_get_source_ip_exclude(NULL);
    assert_false(source_ip_exclude);

    source_ip_exclude = qos_classification_dm_get_source_ip_exclude(classification);
    assert_true(source_ip_exclude);
}

void test_qos_classification_dm_get_interface(UNUSED void** state) {
    amxd_object_t* classification_instance = NULL;
    qos_classification_t* classification = NULL;
    const char* interface = NULL;

    interface = qos_classification_dm_get_interface(NULL);
    assert_null(interface);

    classification_instance = get_classification_instance(1);
    assert_non_null(classification_instance->priv);
    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);
    interface = qos_classification_dm_get_interface(classification);
    assert_non_null(interface);
    assert_string_equal(interface, "eth0");
}

void test_dm_qos_classification_activate_feature_mark_traffic_classes(UNUSED void** state) {
    int retval = -1;
    amxd_object_t* classification_instance = NULL;
    amxd_object_t* queue_instance = NULL;
    qos_classification_t* classification = NULL;
    qos_queue_t* queue = NULL;
    const char* traffic_classes = NULL;
    const char* expected_traffic_classes = "5400,125,92,847";

    classification_instance = get_classification_instance(3);
    queue_instance = get_queue_instance(3);

    assert_non_null(classification_instance);
    assert_non_null(queue_instance);

    classification = (qos_classification_t*) classification_instance->priv;
    assert_non_null(classification);
    assert_false(qos_classification_dm_get_enable(classification));

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    traffic_classes = qos_queue_dm_get_traffic_classes(queue);
    assert_non_null(queue);
    assert_string_equal(traffic_classes, expected_traffic_classes);

    qos_classification_dm_set_enable(classification, true);

    retval = qos_classification_activate(classification);
    assert_int_equal(retval, 0);

    traffic_classes = qos_queue_dm_get_traffic_classes(queue);
    assert_non_null(queue);
    assert_string_equal(traffic_classes, expected_traffic_classes);
}


