/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__QOS_CONTROLLER_H__)
#define __QOS_CONTROLLER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "qos.h"
#include "fwinterface/interface.h"
#include "qos-firewall.h"

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>

amxd_object_t* qos_controller_get_qos_object(void);
qos_t* qos_controller_get_qos(void);

int qos_controller_init(void);
int qos_controller_deinit(void);

int qos_controller_dm_queue_added(amxd_object_t* queue_instance);
int qos_controller_dm_queue_removed(amxd_object_t* queue_instance);
int qos_controller_dm_queue_changed(amxd_object_t* queue_instance);

int qos_controller_dm_shaper_added(amxd_object_t* shaper_instance);
int qos_controller_dm_shaper_removed(amxd_object_t* shaper_instance);
int qos_controller_dm_shaper_changed(amxd_object_t* shaper_instance);

int qos_controller_dm_scheduler_added(amxd_object_t* scheduler_instance);
int qos_controller_dm_scheduler_removed(amxd_object_t* scheduler_instance);
int qos_controller_dm_scheduler_changed(amxd_object_t* scheduler_instance);

int qos_controller_dm_classification_added(amxd_object_t* classification_instance);
int qos_controller_dm_classification_changed(amxd_object_t* classification_instance);
int qos_controller_dm_classification_removed(amxd_object_t* classification_instance);

int qos_controller_dm_queue_stats_added(amxd_object_t* queue_stats_instance);
int qos_controller_dm_queue_stats_changed(amxd_object_t* queue_stats_instance);
int qos_controller_dm_queue_stats_removed(amxd_object_t* queue_stats_instance);

#ifdef __cplusplus
}
#endif

#endif // __QOS_CONTROLLER_H__
