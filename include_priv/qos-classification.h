/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__QOS_CLASSIFICATION_H__)
#define __QOS_CLASSIFICATION_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <fwrules/fw_folder.h>
#include <fwrules/fw_rule.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>

#define QOS_TR181_DEVICE_QOS_CLASSIFICATION_PATH "QoS.Classification"
#define QOS_TR181_DEVICE_QOS_CLASSIFICATION_INSTANCE "QoS.Classification."
#define QOS_TR181_DEVICE_QOS_CLASSIFICATION_OBJECT_NAME "Classification"

//! [architecture classification struct]
typedef struct _qos_classification {
    amxd_object_t* dm_object; /**< The data model object */
    fw_folder_t* folder4;
    fw_folder_t* folder6;
} qos_classification_t;
//! [architecture classification struct]

fw_folder_t* qos_classification_get_folder(const qos_classification_t* const classification);
amxd_object_t* qos_classification_get_dm_object(const qos_classification_t* const classification);
static inline bool qos_classification_dm_get_enable(const qos_classification_t* const classification) {
    return classification == NULL ? false : amxd_object_get_value(bool, classification->dm_object, "Enable", NULL);
}
static inline uint32_t qos_classification_dm_get_order(const qos_classification_t* const classification) {
    return classification == NULL ? 0 : amxd_object_get_value(uint32_t, classification->dm_object, "Order", NULL);
}
static inline const char* qos_classification_dm_get_alias(const qos_classification_t* const classification) {
    return classification == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(classification->dm_object, "Alias"));
}
static inline const char* qos_classification_dm_get_dest_ip(const qos_classification_t* const classification) {
    return classification == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(classification->dm_object, "DestIP"));
}
static inline const char* qos_classification_dm_get_dest_mask(const qos_classification_t* const classification) {
    return classification == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(classification->dm_object, "DestMask"));
}
static inline bool qos_classification_dm_get_dest_ip_exclude(const qos_classification_t* const classification) {
    return classification == NULL ? false : amxd_object_get_value(bool, classification->dm_object, "DestIPExclude", NULL);
}
static inline bool qos_classification_dm_get_source_ip_exclude(const qos_classification_t* const classification) {
    return classification == NULL ? false : amxd_object_get_value(bool, classification->dm_object, "SourceIPExclude", NULL);
}
static inline const char* qos_classification_dm_get_source_ip(const qos_classification_t* const classification) {
    return classification == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(classification->dm_object, "SourceIP"));
}
static inline const char* qos_classification_dm_get_source_mask(const qos_classification_t* const classification) {
    return classification == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(classification->dm_object, "SourceMask"));
}
static inline const char* qos_classification_dm_get_interface(const qos_classification_t* const classification) {
    return classification == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(classification->dm_object, "Interface"));
}
static inline int32_t qos_classification_dm_get_dscp_check(const qos_classification_t* const classification) {
    return classification == NULL ? -1 : amxd_object_get_value(int32_t, classification->dm_object, "DSCPCheck", NULL);
}
static inline int32_t qos_classification_dm_get_protocol(const qos_classification_t* const classification) {
    return classification == NULL ? -1 : amxd_object_get_value(int32_t, classification->dm_object, "Protocol", NULL);
}
static inline bool qos_classification_dm_get_protocol_exclude(const qos_classification_t* const classification) {
    return classification == NULL ? false : amxd_object_get_value(bool, classification->dm_object, "ProtocolExclude", NULL);
}
static inline int32_t qos_classification_dm_get_dest_port(const qos_classification_t* const classification) {
    return classification == NULL ? -1 : amxd_object_get_value(int32_t, classification->dm_object, "DestPort", NULL);
}
static inline int32_t qos_classification_dm_get_dest_port_range_max(const qos_classification_t* const classification) {
    return classification == NULL ? -1 : amxd_object_get_value(int32_t, classification->dm_object, "DestPortRangeMax", NULL);
}
static inline bool qos_classification_dm_get_dest_port_exclude(const qos_classification_t* const classification) {
    return classification == NULL ? false : amxd_object_get_value(bool, classification->dm_object, "DestPortExclude", NULL);
}
static inline int32_t qos_classification_dm_get_source_port(const qos_classification_t* const classification) {
    return classification == NULL ? -1 : amxd_object_get_value(int32_t, classification->dm_object, "SourcePort", NULL);
}
static inline int32_t qos_classification_dm_get_source_port_range_max(const qos_classification_t* const classification) {
    return classification == NULL ? -1 : amxd_object_get_value(int32_t, classification->dm_object, "SourcePortRangeMax", NULL);
}
static inline bool qos_classification_dm_get_source_port_exclude(const qos_classification_t* const classification) {
    return classification == NULL ? false : amxd_object_get_value(bool, classification->dm_object, "SourcePortExclude", NULL);
}
static inline const char* qos_classification_dm_get_source_mac_address(const qos_classification_t* const classification) {
    return classification == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(classification->dm_object, "SourceMACAddress"));
}
static inline bool qos_classification_dm_get_source_mac_exclude(const qos_classification_t* const classification) {
    return classification == NULL ? false : amxd_object_get_value(bool, classification->dm_object, "SourceMACExclude", NULL);
}
static inline int32_t qos_classification_dm_get_dscp_mark(const qos_classification_t* const classification) {
    return classification == NULL ? -1 : amxd_object_get_value(int32_t, classification->dm_object, "DSCPMark", NULL);
}
static inline int32_t qos_classification_dm_get_ethernet_priority_check(const qos_classification_t* const classification) {
    return classification == NULL ? -1 : amxd_object_get_value(int32_t, classification->dm_object, "EthernetPriorityCheck", NULL);
}
static inline int32_t qos_classification_dm_get_ethernet_priority_mark(const qos_classification_t* const classification) {
    return classification == NULL ? -1 : amxd_object_get_value(int32_t, classification->dm_object, "EthernetPriorityMark", NULL);
}
static inline uint32_t qos_classification_dm_get_ip_length_min(const qos_classification_t* const classification) {
    return classification == NULL ? 0 : amxd_object_get_value(uint32_t, classification->dm_object, "IPLengthMin", NULL);
}
static inline uint32_t qos_classification_dm_get_ip_length_max(const qos_classification_t* const classification) {
    return classification == NULL ? 0 : amxd_object_get_value(uint32_t, classification->dm_object, "IPLengthMax", NULL);
}
static inline const char* qos_classification_dm_get_dest_mac_address(const qos_classification_t* const classification) {
    return classification == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(classification->dm_object, "DestMACAddress"));
}
static inline bool qos_classification_dm_get_dest_mac_exclude(const qos_classification_t* const classification) {
    return classification == NULL ? false : amxd_object_get_value(bool, classification->dm_object, "DestMACExclude", NULL);
}
static inline int32_t qos_classification_dm_get_traffic_class(const qos_classification_t* const classification) {
    return classification == NULL ? -1 : amxd_object_get_value(int32_t, classification->dm_object, "TrafficClass", NULL);
}
static inline uint32_t qos_classification_dm_get_forwarding_policy(const qos_classification_t* const classification) {
    return classification == NULL ? 0 : amxd_object_get_value(uint32_t, classification->dm_object, "ForwardingPolicy", NULL);
}
static inline bool qos_classification_dm_get_dscp_exclude(const qos_classification_t* const classification) {
    return classification == NULL ? false : amxd_object_get_value(bool, classification->dm_object, "DSCPExclude", NULL);
}
static inline int32_t qos_classification_dm_get_ip_version(const qos_classification_t* const classification) {
    return classification == NULL ? -1 : amxd_object_get_value(int32_t, classification->dm_object, "IPVersion", NULL);
}
static inline bool qos_classification_dm_get_stoptraverse(const qos_classification_t* const classification) {
    return classification == NULL ? false : amxd_object_get_value(bool, classification->dm_object, "StopTraverse", NULL);
}
static inline uint32_t qos_classification_dm_get_classification_key(const qos_classification_t* const classification) {
    return classification == NULL ? 1 : amxd_object_get_value(uint32_t, classification->dm_object, "ClassificationKey", NULL);
}

int qos_classification_new(qos_classification_t** classification, amxd_object_t* classification_instance);
int qos_classification_init(qos_classification_t* const classification, amxd_object_t* const classification_instance);
int qos_classification_delete(qos_classification_t** classification);
int qos_classification_deinit(qos_classification_t* const classification);

bool fw_rule_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag, const char* chain, const char* table, int index);

int qos_classification_activate(const qos_classification_t* const classification);
int qos_classification_deactivate(const qos_classification_t* const classification);

int qos_classification_dm_set_enable(const qos_classification_t* classification, const bool enable);
int qos_classification_dm_set_order(const qos_classification_t* const classification, const uint32_t order);
int qos_classification_dm_set_dest_ip_exclude(const qos_classification_t* const classification, const bool destipexclude);

#ifdef __cplusplus
}
#endif

#endif // __QOS_CLASSIFICATION_H__
