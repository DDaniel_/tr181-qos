/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__QOS_MODULE_MNGR_H__)
#define __QOS_MODULE_MNGR_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxm/amxm.h>
#include <tr181-qos.h>
#include "qos.h"

/**
   @defgroup qos_module_mngr
   @brief
   QoS Module Manager
 */

#define MOD_QOS_CTRL "qos-ctrl"

int qos_module_mngr_init(void);

/**
   @ingroup qos_module_mngr
   @brief
   Conversion helper function to pass data to a module.

   Creates a new name variant hashtable from an object.
   The returned variant must be freed using amxc_var_delete.

   @param name the name to be used a a key for that variant's hashtable.
   @param object the data model instance.

   @return
   The new variant on success, NULL otherwise.
 */
amxc_var_t* qos_module_build_named_variant(const char* name, amxd_object_t* object);

/**
   @ingroup qos_module_mngr
   @brief
   Conversion helper function to pass data to a module.

   @note
   First, call @ref qos_module_build_named_variant before calling this function.

   Creates a new name variant hashtable from an object and append it to an existing variant.
   The returned variant must be freed using amxc_var_delete.

   Example:
   @code{.c}
   amxc_var_t* data = NULL;
   amxd_object_t* object = get_some_object();
   amxd_object_t* another_object = get_another_object();
   data = qos_module_build_named_variant("example_1", object);
   data = qos_module_build_named_variant_append("example_2", another_object, data);
   // Do something with the variant.
   amxc_var_delete(&data);
   @endcode

   @param name the name to be used a a key for that variant's hashtable.
   @param object the data model instance.
   @param data the exisitng variant where the new variant is added to.

   @return
   The pointer to @ref data is always returned.
 */
amxc_var_t* qos_module_build_named_variant_append(const char* name, amxd_object_t* object, amxc_var_t* data);

#ifdef __cplusplus
}
#endif

#endif //__QOS_MODULE_MNGR_H_
