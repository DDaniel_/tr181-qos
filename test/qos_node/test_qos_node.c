/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include "qos-assert.h"
#include "dm_tr181-qos.h"

#include "test_qos_node.h"
#include "tr181-qos.h"
#include <qoscommon/qos-queue.h>
#include <qosnode/qos-node.h>

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "qos_node_test.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static inline amxd_object_t* get_node_object(void) {
    return amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_NODE_PATH);
}

static amxd_object_t* node_add_instance(const char* alias, const char* reference) {
    int retval = -1;
    amxd_object_t* node_object = NULL;
    amxd_object_t* node_instance = NULL;
    amxc_var_t* parameters = NULL;

    amxc_var_new(&parameters);
    amxc_var_set_type(parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, parameters, "Reference", reference);

    node_object = get_node_object();
    assert_non_null(node_object);

    retval = amxd_object_add_instance(&node_instance, node_object, alias, 0, parameters);
    assert_int_equal(retval, 0);

    amxc_var_delete(&parameters);

    return node_instance;
}

static amxd_object_t* node_add_child_instance(amxd_object_t* node_instance, const char* name) {
    int retval = -1;
    amxd_object_t* node_child_object = NULL;
    amxd_object_t* node_child_instance = NULL;
    amxc_var_t* parameters = NULL;

    amxc_var_new(&parameters);
    amxc_var_set_type(parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, parameters, "Name", name);

    node_child_object = amxd_object_get_child(node_instance, "Child");

    retval = amxd_object_add_instance(&node_child_instance, node_child_object,
                                      NULL, 0, parameters);
    assert_int_equal(retval, 0);
    amxc_var_delete(&parameters);

    return node_instance;
}

static amxd_object_t* node_add_parent_instance(amxd_object_t* node_instance, const char* name) {
    int retval = -1;
    amxd_object_t* node_parent_object = NULL;
    amxd_object_t* node_parent_instance = NULL;
    amxc_var_t* parameters = NULL;

    amxc_var_new(&parameters);
    amxc_var_set_type(parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, parameters, "Name", name);

    node_parent_object = amxd_object_get_child(node_instance, "Parent");

    retval = amxd_object_add_instance(&node_parent_instance, node_parent_object,
                                      NULL, 0, parameters);
    assert_int_equal(retval, 0);
    amxc_var_delete(&parameters);

    return node_instance;
}

int test_qos_node_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_string_equal(QOS_TR181_DEVICE_QOS_NODE_PATH, "QoS.Node");
    assert_string_equal(QOS_TR181_DEVICE_QOS_NODE_INSTANCE, "QoS.Node.");
    assert_string_equal(QOS_TR181_DEVICE_QOS_NODE_OBJECT_NAME, "Node");

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "qos_main",
                                            AMXO_FUNC(_qos_main)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _qos_main(0, &dm, &parser);
    _qos_app_start(NULL, NULL, NULL);
    handle_events();

    return 0;
}

int test_qos_node_teardown(UNUSED void** state) {
    _qos_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_qos_node_new_delete(UNUSED void** state) {
    int retval = -1;
    qos_node_t* node = NULL;
    amxd_object_t* node_instance = NULL;

    node_instance = node_add_instance("test-node-1", "QoS.Queue.queue-1");
    assert_non_null(node_instance);

    retval = qos_node_new(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_node_new(&node, NULL);
    assert_int_equal(retval, -1);
    assert_null(node);

    retval = qos_node_new(NULL, node_instance);
    assert_int_equal(retval, -1);
    assert_null(node_instance->priv);

    retval = qos_node_new(&node, node_instance);
    assert_int_equal(retval, 0);
    assert_non_null(node);
    assert_ptr_equal(node->dm_object, node_instance);
    assert_ptr_equal(node_instance->priv, node);
    assert_true(QOS_NODE_TYPE_QUEUE == node->type);
    assert_non_null(node->data.queue);
    assert_string_equal(qos_queue_dm_get_alias(node->data.queue), "queue-1");
    assert_string_equal(qos_node_dm_get_alias(node), "test-node-1");

    retval = qos_node_delete(NULL);
    assert_int_equal(retval, -1);

    retval = qos_node_delete(&node);
    assert_int_equal(retval, 0);
    assert_null(node);
    assert_null(node_instance->priv);

    amxd_object_delete(&node_instance);
}

void test_qos_node_init_deinit(UNUSED void** state) {
    int retval = -1;
    qos_node_t node;
    amxd_object_t* node_instance = NULL;

    node_instance = node_add_instance("test-node-2", "QoS.Queue.queue-2");
    assert_non_null(node_instance);

    retval = qos_node_init(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_node_init(&node, NULL);
    assert_int_equal(retval, -1);

    retval = qos_node_init(NULL, node_instance);
    assert_int_equal(retval, -1);

    retval = qos_node_init(&node, node_instance);
    assert_int_equal(retval, 0);
    assert_ptr_equal(node.dm_object, node_instance);
    assert_ptr_equal(node_instance->priv, &node);
    assert_non_null(node.data.queue);
    assert_ptr_equal(&node, node.data.queue->node);

    retval = qos_node_deinit(NULL);
    assert_int_equal(retval, -1);

    retval = qos_node_deinit(&node);
    assert_int_equal(retval, 0);
    assert_null(node_instance->priv);
    assert_null(node.data.queue);

    amxd_object_delete(&node_instance);
}

void test_qos_node_get_dm_object(UNUSED void** state) {
    int retval = -1;
    qos_node_t* node = NULL;
    amxd_object_t* node_instance = NULL;

    node_instance = node_add_instance("test-node-3", "QoS.Queue.queue-3");
    assert_non_null(node_instance);

    retval = qos_node_new(&node, node_instance);
    assert_int_equal(retval, 0);

    assert_null(qos_node_get_dm_object(NULL));
    assert_ptr_equal(node_instance, qos_node_get_dm_object(node));

    retval = qos_node_delete(&node);
    assert_int_equal(retval, 0);

    amxd_object_delete(&node_instance);
}

void test_qos_node_dm_get_reference(UNUSED void** state) {
    int retval = -1;
    qos_node_t* node = NULL;
    amxd_object_t* node_instance = NULL;
    const char* reference = NULL;

    node_instance = node_add_instance("test-node-4", "QoS.Queue.queue-4");
    assert_non_null(node_instance);

    retval = qos_node_new(&node, node_instance);
    assert_int_equal(retval, 0);

    reference = qos_node_dm_get_reference(NULL);
    assert_null(reference);

    reference = qos_node_dm_get_reference(node);
    assert_non_null(reference);
    assert_string_equal(reference, "QoS.Queue.queue-4");

    assert_true(QOS_NODE_TYPE_QUEUE == node->type);

    retval = qos_node_delete(&node);
    assert_int_equal(retval, 0);

    amxd_object_delete(&node_instance);
}

void test_qos_node_undefined_reference(UNUSED void** state) {
    //this function checks if it is possible to set a node to an undefined reference
    int retval = -1;
    qos_node_t* node = NULL;
    amxd_object_t* node_instance = NULL;

    node_instance = node_add_instance("test-node-undefined-reference", "QoS.Queue.queue-undefined");
    assert_non_null(node_instance);

    retval = qos_node_new(&node, node_instance);
    assert_int_equal(retval, -1);
    assert_null(node);

    amxd_object_delete(&node_instance);
}

void test_qos_node_parent(UNUSED void** state) {
    int retval = -1;
    qos_node_t* node = NULL;
    qos_node_t* node_parent = NULL;
    amxd_object_t* node_queue_1 = NULL;
    amxd_object_t* node_queue_2 = NULL;
    /**
     * queue-2 has parent queue-1.
     */
    node_queue_1 = node_add_instance("test-node-queue-1", "QoS.Queue.queue-1");
    assert_non_null(node_queue_1);

    node_queue_2 = node_add_instance("test-node-queue-2", "QoS.Queue.queue-2");
    assert_non_null(node_queue_2);
    node_queue_2 = node_add_parent_instance(node_queue_2, "QoS.Node.test-node-queue-1");
    assert_non_null(node_queue_2);
    retval = qos_node_new(&node, node_queue_2);
    assert_int_equal(retval, 0);
    assert_non_null(node);
    //A new parent object has been implicitly created.
    assert_int_equal(amxc_llist_size(node->parents), 1);
    node_parent = qos_node_llist_it_get_node(amxc_llist_get_at(node->parents, 0));
    assert_non_null(node_parent);
    assert_string_equal(qos_node_dm_get_alias(node_parent), "test-node-queue-1");
    assert_int_equal(node_parent->type, QOS_NODE_TYPE_QUEUE);

    assert_int_equal(amxc_llist_size(node_parent->children), 1);
    assert_int_equal(amxc_llist_size(node->children), 0);

    retval = qos_node_delete(&node_parent);
    assert_int_equal(retval, 0);
    assert_null(node_parent);

    retval = qos_node_delete(&node);
    assert_int_equal(retval, 0);
    assert_null(node);

    amxd_object_delete(&node_queue_1);
    amxd_object_delete(&node_queue_2);
}

void test_qos_node_parent_cascade(UNUSED void** state) {
    int retval = -1;
    qos_node_t* node = NULL;
    qos_node_t* node_queue_1 = NULL;
    qos_node_t* node_queue_2 = NULL;
    qos_node_t* node_queue_3 = NULL;
    amxd_object_t* obj_queue_1 = NULL;
    amxd_object_t* obj_queue_2 = NULL;
    amxd_object_t* obj_queue_3 = NULL;

    /**
     * queue-1 <-> queue-2 <-> queue-3
     */
    obj_queue_1 = node_add_instance("test-node-queue-1", "QoS.Queue.queue-1");
    assert_non_null(obj_queue_1);

    obj_queue_2 = node_add_instance("test-node-queue-2", "QoS.Queue.queue-2");
    assert_non_null(obj_queue_2);
    obj_queue_2 = node_add_parent_instance(obj_queue_2, "QoS.Node.test-node-queue-1");
    assert_non_null(obj_queue_2);

    obj_queue_3 = node_add_instance("test-node-queue-3", "QoS.Queue.queue-3");
    assert_non_null(obj_queue_3);
    obj_queue_3 = node_add_parent_instance(obj_queue_3, "QoS.Node.test-node-queue-2");
    assert_non_null(obj_queue_3);

    retval = qos_node_new(&node, obj_queue_3);
    assert_int_equal(retval, 0);
    assert_non_null(node);

    //Parents have been implicitly created.
    node_queue_3 = node;
    assert_non_null(node_queue_3);
    assert_int_equal(amxc_llist_size(node_queue_3->parents), 1);
    node_queue_2 = qos_node_llist_it_get_node(amxc_llist_get_at(node_queue_3->parents, 0));
    assert_non_null(node_queue_2);
    assert_int_equal(amxc_llist_size(node_queue_2->parents), 1);
    node_queue_1 = qos_node_llist_it_get_node(amxc_llist_get_at(node_queue_2->parents, 0));
    assert_non_null(node_queue_1);
    assert_int_equal(amxc_llist_size(node_queue_1->parents), 0);

    retval = qos_node_delete(&node_queue_2);
    assert_int_equal(retval, 0);

    //queue-2 is gone
    assert_null(node_queue_2);
    //queue-1 should not have any children.
    assert_int_equal(amxc_llist_size(node_queue_1->children), 0);
    //queue-3 should not have a parent.
    assert_true(amxc_llist_is_empty(node_queue_3->parents));

    retval = qos_node_delete(&node_queue_1);
    assert_int_equal(retval, 0);

    retval = qos_node_delete(&node_queue_3);
    assert_int_equal(retval, 0);

    amxd_object_delete(&obj_queue_1);
    amxd_object_delete(&obj_queue_2);
    amxd_object_delete(&obj_queue_3);
}

void test_qos_node_children(UNUSED void** state) {
    int retval = -1;
    qos_node_t* node = NULL;
    amxd_object_t* node_queue_1 = NULL;
    amxd_object_t* node_queue_2 = NULL;
    amxd_object_t* node_queue_3 = NULL;
    amxc_llist_t* children = NULL;

    /**
     * queue-1
     *   ├── queue-2
     *   └── queue-3
     */
    node_queue_1 = node_add_instance("test-node-queue-1", "QoS.Queue.queue-1");
    assert_non_null(node_queue_1);

    node_queue_2 = node_add_instance("test-node-queue-2", "QoS.Queue.queue-2");
    assert_non_null(node_queue_2);
    node_queue_2 = node_add_parent_instance(node_queue_2, "QoS.Node.test-node-queue-1");
    assert_non_null(node_queue_2);

    node_queue_3 = node_add_instance("test-node-queue-3", "QoS.Queue.queue-3");
    assert_non_null(node_queue_3);
    node_queue_3 = node_add_parent_instance(node_queue_3, "QoS.Node.test-node-queue-1");
    assert_non_null(node_queue_3);

    //Add two children to queue-1.
    node_queue_1 = node_add_child_instance(node_queue_1, "test-node-queue-2");
    assert_non_null(node_queue_1);

    node_queue_1 = node_add_child_instance(node_queue_1, "test-node-queue-3");
    assert_non_null(node_queue_1);

    retval = qos_node_new(&node, node_queue_1);
    assert_int_equal(retval, 0);
    assert_non_null(node);
    children = qos_node_get_children(node);
    assert_non_null(children);
    assert_true(amxc_llist_is_empty(node->parents));
    assert_int_equal(amxc_llist_size(children), 2);

    amxc_llist_for_each(it, children) {
        qos_node_t* node_child = qos_node_llist_it_get_node(it);
        qos_node_t* node_parent = NULL;

        assert_non_null(node_child);
        assert_int_equal(amxc_llist_size(node_child->parents), 1);
        assert_int_equal(node_child->type, QOS_NODE_TYPE_QUEUE);
        assert_non_null(node_child->data.queue);
        assert_non_null(node_child->dm_object);
        assert_non_null(node_child->dm_object->priv);

        node_parent = qos_node_llist_it_get_node(amxc_llist_get_at(node_child->parents, 0));
        assert_ptr_equal(node_parent, node);

        retval = qos_node_delete(&node_child);
        assert_int_equal(retval, 0);
    }

    retval = qos_node_delete(&node);
    assert_int_equal(retval, 0);

    amxd_object_delete(&node_queue_1);
    amxd_object_delete(&node_queue_2);
    amxd_object_delete(&node_queue_3);
}

void test_qos_node_nested_children(UNUSED void** state) {
    int retval = -1;
    qos_node_t* node = NULL;
    qos_node_t* empty_node = NULL;
    amxd_object_t* node_queue_root = NULL;
    amxd_object_t* node_queue_game = NULL;
    amxd_object_t* node_queue_iptv = NULL;
    amxd_object_t* node_queue_game_1 = NULL;
    amxd_object_t* node_queue_game_2 = NULL;
    amxd_object_t* node_queue_stb = NULL;
    amxd_object_t* node_queue_cast = NULL;

    /**
     * queue-root
     *   ├── queue-game
     *   │    ├─queue-game-1
     *   │    └─queue-game-2
     *   └── queue-iptv
     *        ├─queue-stb
     *        └─queue-cast
     */

    //Level 1
    node_queue_root = node_add_instance("test-node-queue-root", "QoS.Queue.queue-root");
    assert_non_null(node_queue_root);

    //Level 2
    node_queue_game = node_add_instance("test-node-queue-game", "QoS.Queue.queue-game");
    assert_non_null(node_queue_game);
    node_queue_game = node_add_parent_instance(node_queue_game, "QoS.Node.test-node-queue-root");
    assert_non_null(node_queue_game);

    node_queue_iptv = node_add_instance("test-node-queue-iptv", "QoS.Queue.queue-iptv");
    assert_non_null(node_queue_iptv);
    node_queue_iptv = node_add_parent_instance(node_queue_iptv, "QoS.Node.test-node-queue-root");
    assert_non_null(node_queue_iptv);

    //Level 3
    node_queue_game_1 = node_add_instance("test-node-queue-game-1", "QoS.Queue.queue-game-1");
    assert_non_null(node_queue_game_1);
    node_queue_game_1 = node_add_parent_instance(node_queue_game_1, "QoS.Node.test-node-queue-game");
    assert_non_null(node_queue_game_1);

    node_queue_game_2 = node_add_instance("test-node-queue-game-2", "QoS.Queue.queue-game-2");
    assert_non_null(node_queue_game_2);
    node_queue_game_2 = node_add_parent_instance(node_queue_game_2, "QoS.Node.test-node-queue-game");
    assert_non_null(node_queue_game_2);

    node_queue_stb = node_add_instance("test-node-queue-stb", "QoS.Queue.queue-stb");
    assert_non_null(node_queue_stb);
    node_queue_stb = node_add_parent_instance(node_queue_stb, "QoS.Node.test-node-queue-iptv");
    assert_non_null(node_queue_stb);

    node_queue_cast = node_add_instance("test-node-queue-cast", "QoS.Queue.queue-cast");
    assert_non_null(node_queue_cast);
    node_queue_cast = node_add_parent_instance(node_queue_cast, "QoS.Node.test-node-queue-iptv");
    assert_non_null(node_queue_cast);

    //Level 1, children
    node_queue_root = node_add_child_instance(node_queue_root, "test-node-queue-game");
    assert_non_null(node_queue_root);
    node_queue_root = node_add_child_instance(node_queue_root, "test-node-queue-iptv");
    assert_non_null(node_queue_root);

    //Level 2, children, use absolute path for the test.
    node_queue_game = node_add_child_instance(node_queue_game, "QoS.Node.test-node-queue-game-1");
    assert_non_null(node_queue_game);
    node_queue_game = node_add_child_instance(node_queue_game, "QoS.Node.test-node-queue-game-2");
    assert_non_null(node_queue_game);

    //Level 3, children
    node_queue_iptv = node_add_child_instance(node_queue_iptv, "test-node-queue-stb");
    assert_non_null(node_queue_iptv);
    node_queue_iptv = node_add_child_instance(node_queue_iptv, "test-node-queue-cast");
    assert_non_null(node_queue_iptv);

    //It doesn't matter from which QoS.Node instance a node is initialized.
    //As long as the node belongs to the same tree, all nodes are created implicitly.
    retval = qos_node_new(&node, node_queue_root);
    assert_int_equal(retval, 0);
    assert_non_null(node);

    retval = qos_node_delete_tree(&node);
    assert_int_equal(retval, 0);
    assert_null(node);

    //Initialize from child nodes.
    retval = qos_node_new(&node, node_queue_game_2);
    assert_int_equal(retval, 0);
    assert_non_null(node);

    retval = qos_node_delete_tree(&node);
    assert_int_equal(retval, 0);
    assert_null(node);

    retval = qos_node_new(&node, node_queue_game_1);
    assert_int_equal(retval, 0);
    assert_non_null(node);

    retval = qos_node_delete_tree(&node);
    assert_int_equal(retval, 0);
    assert_null(node);

    retval = qos_node_new(&node, node_queue_iptv);
    assert_int_equal(retval, 0);
    assert_non_null(node);

    retval = qos_node_delete_tree(&node);
    assert_int_equal(retval, 0);
    assert_null(node);

    //Extra checks on tree deletion.
    retval = qos_node_delete_tree(NULL);
    assert_int_equal(retval, -1);

    retval = qos_node_delete_tree(&empty_node);
    assert_int_equal(retval, -1);

    amxd_object_delete(&node_queue_root);
    amxd_object_delete(&node_queue_game);
    amxd_object_delete(&node_queue_iptv);
    amxd_object_delete(&node_queue_game_1);
    amxd_object_delete(&node_queue_game_2);
    amxd_object_delete(&node_queue_stb);
    amxd_object_delete(&node_queue_cast);

}

void test_qos_node_nested_children_multiple_parents(UNUSED void** state) {
    int retval = -1;
    qos_node_t* node = NULL;
    qos_node_t* empty_node = NULL;
    amxd_object_t* node_scheduler_low_latency = NULL;
    amxd_object_t* node_scheduler_game = NULL;
    amxd_object_t* node_scheduler_iptv = NULL;
    amxd_object_t* node_scheduler_default = NULL;
    amxd_object_t* node_queue_game_1 = NULL;
    amxd_object_t* node_queue_game_2 = NULL;
    amxd_object_t* node_queue_stb = NULL;
    amxd_object_t* node_queue_cast = NULL;
    amxd_object_t* node_queue_default = NULL;
    amxd_object_t* node_queue_1 = NULL;
    amxd_object_t* node_queue_2 = NULL;

    /**
     * scheduler-low-latency
     *   ├─ scheduler-game
     *   │    ├─queue-game-1
     *   │    └─queue-game-2
     *   │ 
     *   └─ scheduler-iptv
     *        ├─queue-stb
     *        ├─queue-cast
     *        └─queue-default
     *            └─queue-1
     *
     * scheduler-default
     *   ├─ queue-default
     *   │    └─queue-1
     *   └─ queue-2
     *
     *   queue-default has two parents: scheduler-default and scheduler-iptv
     */

    //Level 1
    node_scheduler_low_latency = node_add_instance("test-node-scheduler-low-latency",
                                                   "QoS.Scheduler.scheduler-low-latency");
    assert_non_null(node_scheduler_low_latency);

    node_scheduler_default = node_add_instance("test-node-scheduler-default",
                                               "QoS.Scheduler.scheduler-default");
    assert_non_null(node_scheduler_default);

    //Level 2
    node_scheduler_game = node_add_instance("test-node-scheduler-game", "QoS.Scheduler.scheduler-game");
    assert_non_null(node_scheduler_game);
    node_scheduler_game = node_add_parent_instance(node_scheduler_game,
                                                   "QoS.Node.test-node-scheduler-low-latency");
    assert_non_null(node_scheduler_game);

    node_scheduler_iptv = node_add_instance("test-node-scheduler-iptv", "QoS.Scheduler.scheduler-iptv");
    assert_non_null(node_scheduler_iptv);
    node_scheduler_iptv = node_add_parent_instance(node_scheduler_iptv,
                                                   "QoS.Node.test-node-scheduler-low-latency");
    assert_non_null(node_scheduler_iptv);

    node_queue_default = node_add_instance("test-node-queue-default", "QoS.Queue.queue-default");
    assert_non_null(node_queue_default);
    node_queue_default = node_add_parent_instance(node_queue_default,
                                                  "QoS.Node.test-node-scheduler-default");
    assert_non_null(node_queue_default);

    node_queue_2 = node_add_instance("test-node-queue-2", "QoS.Queue.queue-2");
    assert_non_null(node_queue_2);
    node_queue_2 = node_add_parent_instance(node_queue_2,
                                            "QoS.Node.test-node-scheduler-default");

    //Level 3
    node_queue_game_1 = node_add_instance("test-node-queue-game-1", "QoS.Queue.queue-game-1");
    assert_non_null(node_queue_game_1);
    node_queue_game_1 = node_add_parent_instance(node_queue_game_1, "QoS.Node.test-node-scheduler-game");
    assert_non_null(node_queue_game_1);

    node_queue_game_2 = node_add_instance("test-node-queue-game-2", "QoS.Queue.queue-game-2");
    assert_non_null(node_queue_game_2);
    node_queue_game_2 = node_add_parent_instance(node_queue_game_2, "QoS.Node.test-node-scheduler-game");
    assert_non_null(node_queue_game_2);

    node_queue_stb = node_add_instance("test-node-queue-stb", "QoS.Queue.queue-stb");
    assert_non_null(node_queue_stb);
    node_queue_stb = node_add_parent_instance(node_queue_stb, "QoS.Node.test-node-scheduler-iptv");
    assert_non_null(node_queue_stb);

    node_queue_cast = node_add_instance("test-node-queue-cast", "QoS.Queue.queue-cast");
    assert_non_null(node_queue_cast);
    node_queue_cast = node_add_parent_instance(node_queue_cast, "QoS.Node.test-node-scheduler-iptv");
    assert_non_null(node_queue_cast);

    //Re-use of queue_default, add second parent.
    node_queue_default = node_add_parent_instance(node_queue_default, "QoS.Node.test-node-scheduler-iptv");
    assert_non_null(node_queue_default);

    node_queue_1 = node_add_instance("test-node-queue-1", "QoS.Queue.queue-1");
    assert_non_null(node_queue_1);
    node_queue_1 = node_add_parent_instance(node_queue_1, "QoS.Node.test-node-queue-default");
    assert_non_null(node_queue_1);

    //Level 1, children
    node_scheduler_low_latency = node_add_child_instance(node_scheduler_low_latency, "test-node-scheduler-game");
    assert_non_null(node_scheduler_low_latency);
    node_scheduler_low_latency = node_add_child_instance(node_scheduler_low_latency, "test-node-scheduler-iptv");
    assert_non_null(node_scheduler_low_latency);

    node_scheduler_default = node_add_child_instance(node_scheduler_default, "test-node-queue-default");
    assert_non_null(node_scheduler_default);
    node_scheduler_default = node_add_child_instance(node_scheduler_default, "test-node-queue-2");
    assert_non_null(node_scheduler_default);

    //Level 2, children, use absolute path for the test.
    node_scheduler_game = node_add_child_instance(node_scheduler_game,
                                                  "QoS.Node.test-node-queue-game-1");
    assert_non_null(node_scheduler_game);
    node_scheduler_game = node_add_child_instance(node_scheduler_game,
                                                  "QoS.Node.test-node-queue-game-2");
    assert_non_null(node_scheduler_game);

    //Level 3, children
    node_scheduler_iptv = node_add_child_instance(node_scheduler_iptv, "test-node-queue-stb");
    assert_non_null(node_scheduler_iptv);
    node_scheduler_iptv = node_add_child_instance(node_scheduler_iptv, "test-node-queue-cast");
    assert_non_null(node_scheduler_iptv);
    node_scheduler_iptv = node_add_child_instance(node_scheduler_iptv, "test-node-queue-default");
    assert_non_null(node_scheduler_iptv);

    node_queue_default = node_add_child_instance(node_queue_default, "test-node-queue-1");
    assert_non_null(node_queue_default);

    //It doesn't matter from which QoS.Node instance a node is initialized.
    //As long as the node belongs to the same tree or is linked via the graph, all nodes are created implicitly.
    retval = qos_node_new(&node, node_scheduler_iptv);
    assert_int_equal(retval, 0);
    assert_non_null(node);

    retval = qos_node_delete_tree(&node);
    assert_int_equal(retval, 0);
    assert_null(node);

    //Initialize from child nodes.
    retval = qos_node_new(&node, node_queue_game_2);
    assert_int_equal(retval, 0);
    assert_non_null(node);

    retval = qos_node_delete_tree(&node);
    assert_int_equal(retval, 0);
    assert_null(node);

    retval = qos_node_new(&node, node_queue_game_1);
    assert_int_equal(retval, 0);
    assert_non_null(node);

    retval = qos_node_delete_tree(&node);
    assert_int_equal(retval, 0);
    assert_null(node);

    retval = qos_node_new(&node, node_queue_1);
    assert_int_equal(retval, 0);
    assert_non_null(node);

    retval = qos_node_delete_tree(&node);
    assert_int_equal(retval, 0);
    assert_null(node);

    //Extra checks on tree deletion.
    retval = qos_node_delete_tree(NULL);
    assert_int_equal(retval, -1);

    retval = qos_node_delete_tree(&empty_node);
    assert_int_equal(retval, -1);

    amxd_object_delete(&node_scheduler_game);
    amxd_object_delete(&node_scheduler_iptv);
    amxd_object_delete(&node_scheduler_default);
    amxd_object_delete(&node_queue_game_1);
    amxd_object_delete(&node_queue_game_2);
    amxd_object_delete(&node_queue_stb);
    amxd_object_delete(&node_queue_cast);
    amxd_object_delete(&node_queue_default);
    amxd_object_delete(&node_queue_1);
    amxd_object_delete(&node_queue_2);
}

void test_qos_node_get_parent_child_by_type(UNUSED void** state) {
    int retval = -1;
    qos_node_t* node = NULL;
    qos_node_t* node_parent = NULL;
    amxd_object_t* node_queue_1 = NULL;
    amxd_object_t* node_scheduler_default = NULL;

    /**
     * queue-1 has parent scheduler-default.
     */
    node_queue_1 = node_add_instance("test-node-queue-1", "QoS.Queue.queue-1");
    assert_non_null(node_queue_1);

    node_scheduler_default = node_add_instance("test-node-scheduler-default", "QoS.Scheduler.scheduler-default");
    assert_non_null(node_scheduler_default);

    node_queue_1 = node_add_parent_instance(node_queue_1, "QoS.Node.test-node-scheduler-default");
    assert_non_null(node_queue_1);

    retval = qos_node_new(&node, node_queue_1);
    assert_int_equal(retval, 0);
    assert_non_null(node);

    //Parent.
    node_parent = qos_node_llist_it_get_node(amxc_llist_get_at(node->parents, 0));
    assert_null(qos_node_find_parent_by_type(NULL, QOS_NODE_TYPE_QUEUE));
    assert_null(qos_node_find_parent_by_type(node, QOS_NODE_TYPE_NONE));
    assert_null(qos_node_find_parent_by_type(node, QOS_NODE_TYPE_SHAPER));
    assert_ptr_equal(node_parent, qos_node_find_parent_by_type(node, QOS_NODE_TYPE_SCHEDULER));

    //Child.
    assert_null(qos_node_find_child_by_type(NULL, QOS_NODE_TYPE_QUEUE));
    assert_null(qos_node_find_child_by_type(node_parent, QOS_NODE_TYPE_NONE));
    assert_null(qos_node_find_child_by_type(node_parent, QOS_NODE_TYPE_SCHEDULER));
    assert_ptr_equal(node, qos_node_find_child_by_type(node_parent, QOS_NODE_TYPE_QUEUE));

    retval = qos_node_delete(&node_parent);
    assert_int_equal(retval, 0);
    assert_null(node_parent);

    retval = qos_node_delete(&node);
    assert_int_equal(retval, 0);
    assert_null(node);

    amxd_object_delete(&node_queue_1);
    amxd_object_delete(&node_scheduler_default);
}

void test_qos_node_get_parents_children_by_type(UNUSED void** state) {
    int retval = -1;
    amxd_object_t* node_queue_1 = NULL;
    amxd_object_t* node_queue_2 = NULL;
    amxd_object_t* node_queue_3 = NULL;
    amxd_object_t* node_scheduler_default = NULL;
    amxd_object_t* node_shaper_default = NULL;
    qos_node_t* node = NULL;
    amxc_llist_t* node_list = NULL;

    /**
     * queue-1, queue-2 and queue-3 have parent scheduler-default.
     * scheduler-default has shaper-default as parent.
     */
    node_queue_1 = node_add_instance("test-node-queue-1", "QoS.Queue.queue-1");
    assert_non_null(node_queue_1);

    node_queue_2 = node_add_instance("test-node-queue-2", "QoS.Queue.queue-2");
    assert_non_null(node_queue_2);

    node_queue_3 = node_add_instance("test-node-queue-3", "QoS.Queue.queue-3");
    assert_non_null(node_queue_3);

    node_shaper_default = node_add_instance("test-node-shaper-default", "QoS.Shaper.shaper-default");
    assert_non_null(node_shaper_default);

    node_scheduler_default = node_add_instance("test-node-scheduler-default", "QoS.Scheduler.scheduler-default");
    assert_non_null(node_scheduler_default);

    node_shaper_default = node_add_child_instance(node_shaper_default, "test-node-scheduler-default");
    assert_non_null(node_shaper_default);

    node_scheduler_default = node_add_child_instance(node_scheduler_default, "test-node-queue-1");
    assert_non_null(node_scheduler_default);
    node_scheduler_default = node_add_child_instance(node_scheduler_default, "test-node-queue-2");
    assert_non_null(node_scheduler_default);
    node_scheduler_default = node_add_child_instance(node_scheduler_default, "test-node-queue-3");
    assert_non_null(node_scheduler_default);
    node_scheduler_default = node_add_parent_instance(node_scheduler_default, "test-node-shaper-default");
    assert_non_null(node_scheduler_default);

    node_queue_1 = node_add_parent_instance(node_queue_1, "QoS.Node.test-node-scheduler-default");
    assert_non_null(node_queue_1);

    node_queue_2 = node_add_parent_instance(node_queue_2, "QoS.Node.test-node-scheduler-default");
    assert_non_null(node_queue_2);

    node_queue_3 = node_add_parent_instance(node_queue_3, "QoS.Node.test-node-scheduler-default");
    assert_non_null(node_queue_3);

    retval = qos_node_new(&node, node_scheduler_default);
    assert_int_equal(retval, 0);
    assert_non_null(node);

    assert_null(qos_node_find_parents_by_type(NULL, QOS_NODE_TYPE_QUEUE));
    assert_null(qos_node_find_children_by_type(NULL, QOS_NODE_TYPE_SCHEDULER));

    //No queue stats
    node_list = qos_node_find_children_by_type(node, QOS_NODE_TYPE_QUEUE_STATS);
    assert_null(node_list);

    //Scheduler has no scheduler as parent.
    node_list = qos_node_find_parents_by_type(node, QOS_NODE_TYPE_SCHEDULER);
    assert_null(node_list);

    node_list = qos_node_find_parents_by_type(node, QOS_NODE_TYPE_SHAPER);
    assert_non_null(node_list);
    assert_int_equal(amxc_llist_size(node_list), 1);
    qos_node_llist_delete(&node_list);
    assert_null(node_list);

    node_list = qos_node_find_children_by_type(node, QOS_NODE_TYPE_QUEUE);
    assert_non_null(node_list);
    assert_int_equal(amxc_llist_size(node_list), 3);
    qos_node_llist_delete(&node_list);
    assert_null(node_list);

    retval = qos_node_delete_tree(&node);
    assert_int_equal(retval, 0);
    assert_null(node);

    amxd_object_delete(&node_queue_1);
    amxd_object_delete(&node_queue_2);
    amxd_object_delete(&node_queue_3);
    amxd_object_delete(&node_shaper_default);
    amxd_object_delete(&node_scheduler_default);
}

