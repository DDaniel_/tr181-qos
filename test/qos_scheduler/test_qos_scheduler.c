/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include "qos-assert.h"
#include "dm_tr181-qos.h"

#include "libqosmodule_mock.h"
#include "test_qos_scheduler.h"
#include "tr181-qos.h"
#include <qoscommon/qos-scheduler.h>
#include "dm-qos-scheduler.h"
#include "qos.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "qos_scheduler_test.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static inline amxd_object_t* get_scheduler_object(void) {
    return amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_SCHEDULER_PATH);
}

static amxd_object_t* scheduler_add_instance(const char* alias, const bool enable,
                                             const char* scheduler_algorithm, const int32_t shaping_rate,
                                             const int32_t assured_rate, const char* default_queue) {

    int retval = -1;
    amxd_object_t* scheduler_object = NULL;
    amxd_object_t* scheduler_instance = NULL;
    amxc_var_t* parameters = NULL;

    amxc_var_new(&parameters);
    amxc_var_set_type(parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, parameters, "Enable", enable);

    if(scheduler_algorithm) {
        amxc_var_add_key(cstring_t, parameters, "SchedulerAlgorithm", scheduler_algorithm);
    }

    if(shaping_rate >= 0) {
        amxc_var_add_key(int32_t, parameters, "ShapingRate", shaping_rate);
    }

    if(assured_rate >= 0) {
        amxc_var_add_key(int32_t, parameters, "AssuredRate", assured_rate);
    }

    if(default_queue) {
        amxc_var_add_key(cstring_t, parameters, "DefaultQueue", default_queue);
    }

    scheduler_object = get_scheduler_object();
    assert_non_null(scheduler_object);

    retval = amxd_object_add_instance(&scheduler_instance, scheduler_object, alias, 0, parameters);
    assert_int_equal(retval, 0);
    amxc_var_delete(&parameters);

    return scheduler_instance;
}

static amxd_object_t* get_scheduler_instance(uint32_t index) {
    amxd_object_t* scheduler_object = NULL;
    amxd_object_t* scheduler_instance = NULL;

    scheduler_object = amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_SCHEDULER_PATH);
    assert_non_null(scheduler_object);

    scheduler_instance = amxd_object_get_instance(scheduler_object, NULL, index);
    assert_non_null(scheduler_instance);

    return scheduler_instance;
}

int test_qos_scheduler_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_string_equal(QOS_TR181_DEVICE_QOS_SCHEDULER_PATH, "QoS.Scheduler");
    assert_string_equal(QOS_TR181_DEVICE_QOS_SCHEDULER_INSTANCE, "QoS.Scheduler.");
    assert_string_equal(QOS_TR181_DEVICE_QOS_SCHEDULER_OBJECT_NAME, "Scheduler");

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "qos_main",
                                            AMXO_FUNC(_qos_main)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_scheduler_evt_instance_added",
                                            AMXO_FUNC(_dm_qos_scheduler_evt_instance_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_scheduler_evt_instance_changed",
                                            AMXO_FUNC(_dm_qos_scheduler_evt_instance_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_scheduler_action_on_destroy",
                                            AMXO_FUNC(_dm_qos_scheduler_action_on_destroy)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _qos_main(0, &dm, &parser);
    _qos_app_start(NULL, NULL, NULL);
    handle_events();

    return 0;
}

int test_qos_scheduler_teardown(UNUSED void** state) {
    _qos_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_dm_qos_scheduler_evt_instance_changed(UNUSED void** state) {
    uint32_t index = 1;
    amxc_var_t params;
    amxd_object_t* scheduler_instance = NULL;
    qos_scheduler_t* scheduler = NULL;
    bool enable = false;

    expect_any_always(__wrap_qos_module_exec_api_function, id);
    expect_any_always(__wrap_qos_module_exec_api_function, module_name);
    will_return_always(__wrap_qos_module_exec_api_function, 0);

    scheduler_instance = get_scheduler_instance(index);
    assert_non_null(scheduler_instance);
    scheduler = (qos_scheduler_t*) scheduler_instance->priv;
    assert_non_null(scheduler);

    enable = qos_scheduler_dm_get_enable(scheduler);
    assert_true(enable);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &params, "Enable", false);
    amxd_object_send_changed(scheduler_instance, &params, false);
    amxc_var_clean(&params);

    handle_events();
}

void test_dm_qos_scheduler_evt_instance_added(UNUSED void** state) {
    amxd_object_t* scheduler_object = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;

    scheduler_object = amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_SCHEDULER_PATH);
    assert_non_null(scheduler_object);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);


    assert_int_equal(amxd_object_invoke_function(scheduler_object, "_add", &args, &ret), amxd_status_ok);
    assert_false(amxc_var_is_null(&ret));

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_dm_qos_scheduler_action_on_destroy(UNUSED void** state) {
    uint32_t index = 1;
    amxd_object_t* scheduler_instance = NULL;
    qos_scheduler_t* scheduler = NULL;

    scheduler_instance = get_scheduler_instance(index);
    assert_non_null(scheduler_instance);
    scheduler = (qos_scheduler_t*) scheduler_instance->priv;
    assert_non_null(scheduler);

    amxd_object_delete(&scheduler_instance);
    assert_null(scheduler_instance);
}

void test_qos_scheduler_new_delete(UNUSED void** state) {
    int retval = -1;
    qos_scheduler_t* scheduler = NULL;
    qos_scheduler_t* empty_scheduler = NULL;
    qos_scheduler_t* non_empty_scheduler = (qos_scheduler_t*) 0xdeadbeef;
    amxd_object_t* scheduler_instance = NULL;
    amxd_object_t in_use_scheduler_instance = { .priv = (void*) 0xdeadbeef };

    scheduler_instance = scheduler_add_instance("test-scheduler-1", true, "SP", 50000, 35000, "QoS.Queue.test-queue-1");
    assert_non_null(scheduler_instance);

    retval = qos_scheduler_new(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_scheduler_new(&scheduler, NULL);
    assert_int_equal(retval, -1);
    assert_null(scheduler);

    retval = qos_scheduler_new(NULL, scheduler_instance);
    assert_int_equal(retval, -1);
    assert_null(scheduler_instance->priv);

    retval = qos_scheduler_new(&non_empty_scheduler, scheduler_instance);
    assert_int_equal(retval, -1);
    assert_null(scheduler_instance->priv);

    retval = qos_scheduler_new(&scheduler, &in_use_scheduler_instance);
    assert_int_equal(retval, -1);
    assert_null(scheduler);

    retval = qos_scheduler_new(&scheduler, scheduler_instance);
    assert_int_equal(retval, 0);
    assert_non_null(scheduler);
    assert_ptr_equal(scheduler->dm_object, scheduler_instance);
    assert_ptr_equal(scheduler_instance->priv, scheduler);

    retval = qos_scheduler_delete(NULL);
    assert_int_equal(retval, -1);

    retval = qos_scheduler_delete(&empty_scheduler);
    assert_int_equal(retval, -1);

    retval = qos_scheduler_delete(&scheduler);
    assert_int_equal(retval, 0);
    assert_null(scheduler);
    assert_null(scheduler_instance->priv);

    amxd_object_delete(&scheduler_instance);
}

void test_qos_scheduler_init_deinit(UNUSED void** state) {
    int retval = -1;
    qos_scheduler_t scheduler = {0};
    qos_scheduler_t empty_scheduler = {0};
    amxd_object_t* scheduler_instance = NULL;
    amxd_object_t in_use_scheduler_instance = { .priv = (void*) 0xdeadbeef };

    scheduler_instance = scheduler_add_instance("test-scheduler-2", true, "WRR", 50000, 35000, "QoS.Queue.test-queue-2");
    assert_non_null(scheduler_instance);

    retval = qos_scheduler_init(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_scheduler_init(&scheduler, NULL);
    assert_int_equal(retval, -1);
    assert_null(scheduler.dm_object);

    retval = qos_scheduler_init(NULL, scheduler_instance);
    assert_int_equal(retval, -1);
    assert_null(scheduler_instance->priv);

    retval = qos_scheduler_init(&scheduler, &in_use_scheduler_instance);
    assert_int_equal(retval, -1);
    assert_null(scheduler.dm_object);

    retval = qos_scheduler_init(&scheduler, scheduler_instance);
    assert_int_equal(retval, 0);
    assert_ptr_equal(scheduler.dm_object, scheduler_instance);
    assert_ptr_equal(scheduler_instance->priv, &scheduler);

    retval = qos_scheduler_deinit(NULL);
    assert_int_equal(retval, -1);

    retval = qos_scheduler_deinit(&empty_scheduler);
    assert_int_equal(retval, 0);

    retval = qos_scheduler_deinit(&scheduler);
    assert_int_equal(retval, 0);
    assert_null(scheduler.dm_object);
    assert_null(scheduler_instance->priv);

    amxd_object_delete(&scheduler_instance);
}

void test_qos_scheduler_get_dm_object(UNUSED void** state) {
    int retval = -1;
    qos_scheduler_t* scheduler = NULL;
    amxd_object_t* scheduler_instance = NULL;

    scheduler_instance = scheduler_add_instance("test-scheduler-3", true, "WRR", 50000, 35000, "QoS.Queue.test-queue-2");
    assert_non_null(scheduler_instance);

    retval = qos_scheduler_new(&scheduler, scheduler_instance);
    assert_int_equal(retval, 0);

    assert_null(qos_scheduler_get_dm_object(NULL));
    assert_ptr_equal(qos_scheduler_get_dm_object(scheduler), scheduler_instance);

    retval = qos_scheduler_delete(&scheduler);
    assert_int_equal(retval, 0);

    amxd_object_delete(&scheduler_instance);
}

void test_qos_scheduler_dm_get_set_enable(UNUSED void** state) {
    int retval = -1;
    qos_scheduler_t* scheduler = NULL;
    amxd_object_t* scheduler_instance = NULL;

    scheduler_instance = scheduler_add_instance("test-scheduler-3", true, "WRR", 50000, 35000, "QoS.Queue.test-queue-2");
    assert_non_null(scheduler_instance);

    retval = qos_scheduler_new(&scheduler, scheduler_instance);
    assert_int_equal(retval, 0);

    assert_false(qos_scheduler_dm_get_enable(NULL));
    assert_true(qos_scheduler_dm_get_enable(scheduler));

    assert_int_equal(qos_scheduler_dm_set_enable(NULL, true), -1);
    assert_int_equal(qos_scheduler_dm_set_enable(scheduler, false), 0);
    assert_false(qos_scheduler_dm_get_enable(scheduler));
    assert_int_equal(qos_scheduler_dm_set_enable(scheduler, true), 0);
    assert_true(qos_scheduler_dm_get_enable(scheduler));

    retval = qos_scheduler_delete(&scheduler);
    assert_int_equal(retval, 0);

    amxd_object_delete(&scheduler_instance);
}

void test_qos_scheduler_dm_get_set_rates(UNUSED void** state) {
    int retval = -1;
    qos_scheduler_t* scheduler = NULL;
    amxd_object_t* scheduler_instance = NULL;

    scheduler_instance = scheduler_add_instance("test-scheduler-3", true, "WRR", 60000, 40000, "QoS.Queue.test-queue-2");
    assert_non_null(scheduler_instance);

    retval = qos_scheduler_new(&scheduler, scheduler_instance);
    assert_int_equal(retval, 0);

    assert_false(qos_scheduler_dm_get_enable(NULL));
    assert_true(qos_scheduler_dm_get_enable(scheduler));

    assert_int_equal(qos_scheduler_dm_get_shaping_rate(NULL), -1);
    assert_int_equal(qos_scheduler_dm_get_shaping_rate(scheduler), 60000);
    assert_int_equal(qos_scheduler_dm_get_assured_rate(NULL), -1);
    assert_int_equal(qos_scheduler_dm_get_assured_rate(scheduler), 40000);

    assert_int_equal(qos_scheduler_dm_set_shaping_rate(NULL, 1000), -1);
    assert_int_equal(qos_scheduler_dm_set_shaping_rate(scheduler, 55000), 0);
    assert_int_equal(qos_scheduler_dm_get_shaping_rate(scheduler), 55000);
    assert_int_equal(qos_scheduler_dm_set_shaping_rate(scheduler, -2000), 0);
    assert_int_equal(qos_scheduler_dm_get_shaping_rate(scheduler), -1);
    assert_int_equal(qos_scheduler_dm_set_assured_rate(NULL, 2000), -1);
    assert_int_equal(qos_scheduler_dm_set_assured_rate(scheduler, 35000), 0);
    assert_int_equal(qos_scheduler_dm_get_assured_rate(scheduler), 35000);
    assert_int_equal(qos_scheduler_dm_set_assured_rate(scheduler, -6500), 0);
    assert_int_equal(qos_scheduler_dm_get_assured_rate(scheduler), -1);

    retval = qos_scheduler_delete(&scheduler);
    assert_int_equal(retval, 0);

    amxd_object_delete(&scheduler_instance);
}

void test_qos_scheduler_dm_get_alias(UNUSED void** state) {
    int retval = -1;
    qos_scheduler_t* scheduler = NULL;
    amxd_object_t* scheduler_instance = NULL;

    scheduler_instance = scheduler_add_instance("test-scheduler-4", true, "WFQ", 60000, 40000, "QoS.Queue.test-queue-2");
    assert_non_null(scheduler_instance);

    retval = qos_scheduler_new(&scheduler, scheduler_instance);
    assert_int_equal(retval, 0);

    assert_null(qos_scheduler_dm_get_alias(NULL));
    assert_string_equal(qos_scheduler_dm_get_alias(scheduler), "test-scheduler-4");

    retval = qos_scheduler_delete(&scheduler);
    assert_int_equal(retval, 0);

    amxd_object_delete(&scheduler_instance);
}

void test_qos_scheduler_dm_get_default_queue(UNUSED void** state) {
    int retval = -1;
    qos_scheduler_t* scheduler = NULL;
    amxd_object_t* scheduler_instance = NULL;

    scheduler_instance = scheduler_add_instance("test-scheduler-5", true, "WFQ", 60000, 40000, "QoS.Queue.test-queue-3");
    assert_non_null(scheduler_instance);

    retval = qos_scheduler_new(&scheduler, scheduler_instance);
    assert_int_equal(retval, 0);

    assert_null(qos_scheduler_dm_get_default_queue(NULL));
    assert_string_equal(qos_scheduler_dm_get_default_queue(scheduler), "QoS.Queue.test-queue-3");

    retval = qos_scheduler_delete(&scheduler);
    assert_int_equal(retval, 0);

    amxd_object_delete(&scheduler_instance);
}

void test_qos_scheduler_interface_netdevname_changed(UNUSED void** state) {
    amxc_var_t data;
    qos_scheduler_t* scheduler;
    amxd_object_t* scheduler_instance;

    expect_any_always(__wrap_qos_module_exec_api_function, id);
    expect_any_always(__wrap_qos_module_exec_api_function, module_name);
    will_return_always(__wrap_qos_module_exec_api_function, 0);

    assert_int_equal(amxc_var_init(&data), 0);
    assert_int_equal(amxc_var_set_type(&data, AMXC_VAR_ID_CSTRING), 0);

    scheduler_instance = get_scheduler_instance(1);
    assert_non_null(scheduler_instance->priv);

    scheduler = (qos_scheduler_t*) scheduler_instance->priv;
    assert_non_null(scheduler);

    qos_scheduler_interface_netdevname_changed(NULL, NULL, NULL);
    qos_scheduler_interface_netdevname_changed(NULL, NULL, scheduler);

    assert_int_equal(amxc_var_set_cstring_t(&data, ""), 0);
    qos_scheduler_interface_netdevname_changed(NULL, &data, NULL);
    qos_scheduler_interface_netdevname_changed(NULL, &data, scheduler);

    assert_string_equal(scheduler->intf_name, "");

    assert_int_equal(amxc_var_set_cstring_t(&data, "eth0"), 0);
    qos_scheduler_interface_netdevname_changed(NULL, &data, scheduler);

    assert_string_equal(scheduler->intf_name, "eth0");

    scheduler_instance = get_scheduler_instance(2);
    assert_non_null(scheduler_instance->priv);

    scheduler = (qos_scheduler_t*) scheduler_instance->priv;
    assert_non_null(scheduler);

    qos_scheduler_interface_netdevname_changed(NULL, NULL, NULL);
    qos_scheduler_interface_netdevname_changed(NULL, NULL, scheduler);

    assert_int_equal(amxc_var_set_cstring_t(&data, ""), 0);
    qos_scheduler_interface_netdevname_changed(NULL, &data, NULL);
    qos_scheduler_interface_netdevname_changed(NULL, &data, scheduler);

    assert_string_equal(scheduler->intf_name, "");

    assert_int_equal(amxc_var_set_cstring_t(&data, "eth0"), 0);
    qos_scheduler_interface_netdevname_changed(NULL, &data, scheduler);

    assert_string_equal(scheduler->intf_name, "eth0");

    amxc_var_clean(&data);
}

void test_qos_scheduler_activate(UNUSED void** state) {
    int retval = -1;
    qos_scheduler_t* scheduler;
    amxd_object_t* scheduler_instance;
    struct _qos_node* node;

    scheduler_instance = get_scheduler_instance(2);
    assert_non_null(scheduler_instance->priv);

    scheduler = (qos_scheduler_t*) scheduler_instance->priv;
    assert_non_null(scheduler);
    node = scheduler->node;

    // return true on when_null catch
    retval = qos_scheduler_activate(NULL);
    assert_int_equal(retval, -1);

    // return true on when_null catch
    scheduler->node = NULL;
    retval = qos_scheduler_activate(scheduler);
    assert_int_equal(retval, -1);

    //__wrap_qos_module_exec_api_function fails with -1
    scheduler->node = node;
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_ACTIVATE_SCHEDULER);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, -1);
    retval = qos_scheduler_activate(scheduler);
    assert_int_equal(retval, -1);

    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_ACTIVATE_SCHEDULER);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, 11);
    retval = qos_scheduler_activate(scheduler);
    assert_int_equal(retval, -1);

    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_ACTIVATE_SCHEDULER);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, 0);
    retval = qos_scheduler_activate(scheduler);
    assert_int_equal(retval, 0);
}

void test_qos_scheduler_deactivate(UNUSED void** state) {
    int retval = -1;
    qos_scheduler_t* scheduler;
    amxd_object_t* scheduler_instance;
    struct _qos_node* node;

    scheduler_instance = get_scheduler_instance(2);
    assert_non_null(scheduler_instance->priv);

    scheduler = (qos_scheduler_t*) scheduler_instance->priv;
    assert_non_null(scheduler);
    node = scheduler->node;
    // return true on when_null catch
    retval = qos_scheduler_deactivate(NULL);
    assert_int_equal(retval, -1);

    // return true on when_null catch
    scheduler->node = NULL;
    retval = qos_scheduler_deactivate(scheduler);
    assert_int_equal(retval, -1);

    //__wrap_qos_module_exec_api_function fails with -1
    scheduler->node = node;
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_DEACTIVATE_SCHEDULER);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, -1);
    retval = qos_scheduler_deactivate(scheduler);
    assert_int_equal(retval, -1);

    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_DEACTIVATE_SCHEDULER);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, 11);
    retval = qos_scheduler_deactivate(scheduler);
    assert_int_equal(retval, -1);

    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_DEACTIVATE_SCHEDULER);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, 0);
    retval = qos_scheduler_deactivate(scheduler);
    assert_int_equal(retval, 0);
}