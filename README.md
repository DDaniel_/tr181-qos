# tr181-qos

[TOC]

# Introduction

`tr181-qos` implements the TR-181 Device.QoS data model. The data model is
based on *TR-181 Issue: 2 Amendment 16* and the specification can be found on the
[BroadBand Forum](https://www.broadband-forum.org/)'s website. The data model
is supported by protocols like
[CWMP](https://cwmp-data-models.broadband-forum.org/) or
[USP](https://usp-data-models.broadband-forum.org/).

TR-181 Device.QoS defines objects and parameters to describe Quality of Service
(QoS) configurations on the CPE. These objects and parameters form a subset of
the *Device:2* root data model and can be consulted:
- [Root data model for
  CWMP](https://cwmp-data-models.broadband-forum.org/#Device:2)
- [Root data model for
  USP](https://usp-data-models.broadband-forum.org/#Device:2)

Different parts of a QoS configuration can be described using the data model.
These parts are divided into classifications, queues, shapers, schedulers,
etc.

TR-181 Device.QoS defines the following objects:

| Object                            | Implemented in tr181-qos |
| --------------------------------- | ------------------------ |
| Device.QoS.                       | **Yes**                  |
| Device.QoS.Classification.{i}.    | **Yes, a subset**        |
| Device.QoS.App.{i}.               | No                       |
| Device.QoS.Flow.{i}.              | No                       |
| Device.QoS.Policer.{i}.           | No                       |
| Device.QoS.Queue.{i}.             | **Yes**                  |
| Device.QoS.QueueStats.{i}.        | **Yes**                  |
| Device.QoS.Shaper.{i}.            | **Yes**                  |
| Device.QoS.Scheduler.{i}.         | **Yes**                  |

@attention
This implementation also defines custom objects, which are not part of TR-181.

| Custom Object                     |
| --------------------------------- |
| Device.QoS.Node.{i}.              |

The use of all objects is described futher in this documentation.

# Overview

The next image shows a high level overview of tr181-qos. From top to bottom,
four layers exist:
 - Data model layer.
 - Core.
 - Modules.
 - Libraries.

The layers are explained briefly below the image. For more details, check the
[architecture](@ref architecture) page.

![High level overview](doc/images/high_level_overview.svg)

## Data model layer

A subset of the TR-181 Device.QoS data model specification as described by the
BroadBand Forum. The data model is defined by the Object Definition Language
(ODL). In general, one ODL file exists for each object. E.g.
Device.QoS.Classification, Device.QoS.Queue, etc.

## Core

The core refers to tr181-qos, the component described in this documentation.
The core can be split in several parts:

- **Ambiorix framework**: the core is built on top of the `Ambiorix (AMX)
  framework`.  Ambiorix consists of several libraries, providing data
  collections, data model parsers and the ability to connect to different bus
  systems. A good comprehension of AMX is required to leverage all of its
  features. Consult the Ambiorix documentation for more info.

* **QoS manager**: this block is at the heart of the core application,
  interacting with the data model via Ambiorix and the lower level layers.  The
  QoS manager has several responsibilities:

    - Data model event handling, e.g. addition/deletion/altering of data model
      instances.
    - Update the data model's state.
    - Convert the data model's parameters into a set of parameters that can
      be used by the lower level layers.
    - Network event handling via `libnetmodel`. E.g., an interface can
      disappear and as as consequence, a particular classification, queue can
      be disabled.

- **Classification**: the classification block performs the translation from a
  Device.QoS.Classification.{i} instance to lower level firewall parameters,
  which is used to update firewall rules. To achieve this, two additional
  libraries, `libfwrules` and `libfwinterface` are used. Both libraries are not
  drawn in the image, but can be found at the [architecture](@ref architecture)
  page.

- **Queue/Shaper/Scheduler/QueueStats**: this block can be split in
  multiple blocks, but in essence, they share the same tasks:
    - Convert the data model instance parameters to a format, suitable to be
      processed by the lower layer modules.
    - Dispatch tasks to the lower layer modules, for example, to activate a
      queue, reconfigure a shaper or query queue statistics.
    - Notify the QoS manager about the status of an object to update the data
      model accordingly.

- **libqoscommon**: library grouping common functionality for Shapers,
  Schedulers, Queues and QueueStats.

- **libqosnode**: library providing an abstraction layer for Shapers,
  Schedulers, Queues and QueueStats, and the possibility to have a hierarchical
  QoS configuration.

## Modules

Modules are standalone plugins (shared object files) and are loaded at startup.
A module implements the low-level API, used to apply QoS to the system. A
default module for Linux Traffic Control is available. Other modules can be
developed as well, which can, for example, implement a proprietary interface
for QoS. Multiple modules can run next to each other. There are no limitations.

The list of modules to load at startup can be set by the comma-separated
`SupportedControllers` parameter, found in the configuration ODL defaults file.

Each data model instance uses the Linux Traffic Control module by default, but
this can be overwritten by defining a `Controller` parameter for that instance.
The `Controller` parameter value must be present in the `SupportedControllers`
parameter list.

More info can be found at the [module](@ref module) page.

## Libraries

The grey blocks represent the libraries, provided by the open source community
or the hardware vendor. The modules leverage these libraries to interface with
the operating system and the underlying hardware.

# Building, installing and testing

## Docker container

You could install all tools needed for testing and developing on your local
machine, or use a pre-configured environment. Such an environment is already
prepared for you as a docker container.

1. Install Docker

    Docker must be installed on your system. Here are some links that could
    help you:

    - [Get Docker Engine - Community for
      Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for
      Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for
      Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for
      CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
      <br /><br />

    Make sure you user id is added to the docker group:

        sudo usermod -aG docker $USER
    <br />

2. Fetch the container image

    To get access to the pre-configured environment, pull the image and launch
    a container.

    Pull the image:

        docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    Before launching the container, you should create a directory which will be
    shared between your local machine and the container.

        mkdir -p ~/amx/qos/applications/

    Launch the container:

        docker run -ti -d \
            --name oss-dbg \
            --restart=always \
            --cap-add=SYS_PTRACE \
            --cap-add=CAP_NET_RAW \
            --cap-add=CAP_NET_ADMIN \
            --sysctl net.ipv6.conf.all.disable_ipv6=1 \
            -e "USER=$USER" \
            -e "UID=$(id -u)" \
            -e "GID=$(id -g)" \
            -v ~/amx:/home/$USER/amx \
            registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    The `-v` option bind mounts the local directory for the amx project in
    the container, at the exact same place.
    The `-e` options create environment variables in the container. These
    variables are used to create a user name with exactly the same user id and
    group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

        docker exec -ti --user $USER oss-dbg /bin/bash

## Building

### Prerequisites

<code>tr181-qos</code> depends on the following libraries:

- libamxc
- libamxo
- libamxp
- libamxd
- libamxm
- libsahtrace
- libfwrules
- libfwinterface

These libraries and additional packages can be installed in the container:

    sudo apt-get update
    sudo apt-get install libamxc libamxo libamxp libamxd libamxm amxo-cg
    amxo-xml-to amxrt mod_dmext mod-amxb-ubus mod-ba-cli mod-dm-cli
    mod-sahtrace sah-lib-sahtrace-dev libfwrules libfwinterface libnetmodel
    iptables iproute2

The `iproute2` is installed for later purposes and provides the `ip` command.

### Build tr181-qos

1. Clone the git repository

    To be able to build it, you need the source code. So open the directory
    just created for the ambiorix project and clone this library in it (on your
    local machine).

        cd ~/amx/qos/applications/
        git clone git@gitlab.com:prpl-foundation/components/core/plugins/tr181-qos.git

2. Build it

    When using the internal gitlab, you must define an environment variable
    `VERSION_PREFIX` before building in your docker container.

        export VERSION_PREFIX="master_"

    After the variable is set, you can build the package.

        cd ~/amx/qos/applications/tr181-qos
        make

## Installing

### Using make target install

You can install your own compiled version easily in the container by running
the install target.

    cd ~/amx/qos/applications/tr181-qos
    sudo -E make install

### Using package

From within the container you can create packages.

    cd ~/amx/qos/applications/tr181-qos
    make package

The packages generated are:

    ~/amx/qos/applications/tr181-qos/tr181-qos-<VERSION>.tar.gz
    ~/amx/qos/applications/tr181-qos/tr181-qos-<VERSION>.deb

You can copy these packages and extract/install them.

For Ubuntu or Debian distributions use dpkg:

    sudo dpkg -i ~/amx/qos/applications/tr181-qos/tr181-qos-<VERSION>.deb

## Testing

### Prerequisites

No extra components are needed for testing `tr181-qos`.

### Run tests

You can run the tests by executing the following command:

    cd ~/amx/qos/applications/tr181-qos/test
    make

Or this command if you also want the coverage tests to run:

    cd ~/amx/qos/applications/tr181-qos/tests
    make run coverage

You can combine both commands:

    cd ~/amx/qos/applications/tr181-qos
    make test

### Coverage reports

The coverage target will generate coverage reports using
[gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and
[gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each c-file is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are
available in the output directory of the compiler used.  For example, when using
native gcc, the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML
coverage reports can be found at
`~/amx/qos/applications/tr181-qos/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser. In the container start a
python3 http server in background.

    cd ~/amx/
    python3 -m http.server 8080 &

Use the following url to access the reports:

    http://<IP ADDRESS OF YOUR CONTAINER>:8080/qos/applications/tr181-qos/output/<MACHINE>/coverage/report

You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/qos/applications/tr181-qos$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is:
`http://172.17.0.3:8080/qos/applications/tr181-qos/output/x86_64-linux-gnu/coverage/report/`

## Documentation

### Prerequisites

To generate the documentation, [Doxygen](https://www.doxygen.nl) is required
and already available in the container. In case you want to install this on
your local machine:

    sudo apt-get install doxygen

### Paths

The documentation is split in two parts, starting from the repository path:

    cd ~/amx/qos/applications/tr181-qos

Here, you can find:

- README.md: this file is used on Gitlab and is the main page for the Doxygen documentation.
- docs directory: contains the Doxygen settings, code examples and additional pages used in Doxygen.

The code itself is documented in the approriate header and source files.

### Generate documentation

You can generate the documentation by executing the following command:

    cd ~/amx/qos/applications/tr181-qos
    make doc

The full documentation is available in the `output` directory. You can easily
access the documentation in your browser. As described in the coverage reports
section, you can start a http server in the container.

    cd ~/amx/
    python3 -m http.server 8080 &

Note that there are two different kinds of documentation:
- Doxygen documentation, describing the internals of the TR-181 QoS Manager.
- ODL documentation, describing the TR-181 Device.QoS data model.

Use the following urls to access the reports:

- Doxygen documentation:

        http://<IP ADDRESS OF YOUR CONTAINER>:8080/qos/applications/tr181-qos/output/doc/html/index.html

- TR-181 data model documentation:

        http://<IP ADDRESS OF YOUR CONTAINER>:8080/qos/applications/tr181-qos/output/html/index.html


You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/qos/applications/tr181-qos$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is
`http://172.17.0.3:8080/qos/applications/tr181-qos/output/doc/html/index.html`
or
`http://172.17.0.3:8080/qos/applications/tr181-qos/output/html/index.html`

# What's next?

Consult the full documentation for a complete description of the architecture,
the module API, example configurations, etc.

