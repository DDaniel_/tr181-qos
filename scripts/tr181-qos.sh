#!/bin/sh
[ -f /etc/environment ] && source /etc/environment
ulimit -c ${ULIMIT_CONFIGURATION:-0}

case $1 in
    start|boot)
	tr181-qos -D
        ;;
    stop)
        if [ -f /var/run/tr181-qos.pid ]; then
            kill `cat /var/run/tr181-qos.pid`
        fi
        ;;
    debuginfo)
	ubus-cli "QoS.?"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    log)
	echo "TODO log qos manager"
	;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac
