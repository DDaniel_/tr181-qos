/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include "qos-assert.h"
#include "dm_tr181-qos.h"

#include "libqosmodule_mock.h"
#include "test_qos_queue_stats.h"
#include "tr181-qos.h"
#include <qoscommon/qos-queue-stats.h>
#include "dm-qos-queue-stats.h"
#include "qos.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "qos_queue_stats_test.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static inline amxd_object_t* get_queue_stats_object(void) {
    return amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_QUEUE_STATS_PATH);
}

static amxd_object_t* queue_stats_add_instance(const char* alias, const bool enable,
                                               const char* interface, const char* queue) {
    int retval = -1;
    amxd_object_t* queue_stats_object = NULL;
    amxd_object_t* queue_stats_instance = NULL;
    amxc_var_t* parameters = NULL;

    amxc_var_new(&parameters);
    amxc_var_set_type(parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, parameters, "Enable", enable);

    if(queue) {
        amxc_var_add_key(cstring_t, parameters, "Queue", queue);
    }

    if(interface) {
        amxc_var_add_key(cstring_t, parameters, "Interface", interface);
    }

    queue_stats_object = get_queue_stats_object();
    assert_non_null(queue_stats_object);

    retval = amxd_object_add_instance(&queue_stats_instance, queue_stats_object, alias, 0, parameters);
    assert_int_equal(retval, 0);

    amxc_var_delete(&parameters);

    return queue_stats_instance;
}

//Use this function if you want to retrieve an instance by index from the ODL file.
static amxd_object_t* get_queue_stats_instance(uint32_t index) {
    amxd_object_t* queue_stats_object = NULL;
    amxd_object_t* queue_stats_instance = NULL;

    queue_stats_object = amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_QUEUE_STATS_PATH);
    assert_non_null(queue_stats_object);

    queue_stats_instance = amxd_object_get_instance(queue_stats_object, NULL, index);
    assert_non_null(queue_stats_instance);

    return queue_stats_instance;
}

int test_qos_queue_stats_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "qos_main",
                                            AMXO_FUNC(_qos_main)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_queue_stats_evt_instance_added",
                                            AMXO_FUNC(_dm_qos_queue_stats_evt_instance_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_queue_stats_evt_instance_changed",
                                            AMXO_FUNC(_dm_qos_queue_stats_evt_instance_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_queue_stats_action_on_read",
                                            AMXO_FUNC(_dm_qos_queue_stats_action_on_read)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_queue_stats_action_on_destroy",
                                            AMXO_FUNC(_dm_qos_queue_stats_action_on_destroy)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _qos_main(0, &dm, &parser);
    _qos_app_start(NULL, NULL, NULL);
    handle_events();

    return 0;
}

int test_qos_queue_stats_teardown(UNUSED void** state) {
    _qos_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_dm_qos_queue_stats_evt_instance_changed(UNUSED void** state) {
    uint32_t index = 1;
    amxc_var_t params;
    amxd_object_t* queue_stats_instance = NULL;
    qos_queue_stats_t* queue_stats = NULL;
    bool enable = false;

    queue_stats_instance = get_queue_stats_instance(index);
    assert_non_null(queue_stats_instance);
    queue_stats = (qos_queue_stats_t*) queue_stats_instance->priv;
    assert_non_null(queue_stats);

    enable = qos_queue_stats_dm_get_enable(queue_stats);
    assert_true(enable);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &params, "Enable", false);
    amxd_object_send_changed(queue_stats_instance, &params, false);
    amxc_var_clean(&params);

    handle_events();

}

void test_dm_qos_queue_stats_evt_instance_added(UNUSED void** state) {
    amxd_object_t* queue_stats_object = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;

    queue_stats_object = amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_QUEUE_STATS_PATH);
    assert_non_null(queue_stats_object);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);


    assert_int_equal(amxd_object_invoke_function(queue_stats_object, "_add", &args, &ret), amxd_status_ok);
    assert_false(amxc_var_is_null(&ret));

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_dm_qos_queue_stats_action_on_destroy(UNUSED void** state) {
    uint32_t index = 1;
    amxd_object_t* queue_stats_instance = NULL;
    qos_queue_stats_t* queue_stats = NULL;

    queue_stats_instance = get_queue_stats_instance(index);
    assert_non_null(queue_stats_instance);
    queue_stats = (qos_queue_stats_t*) queue_stats_instance->priv;
    assert_non_null(queue_stats);

    amxd_object_delete(&queue_stats_instance);
    assert_null(queue_stats_instance);
}

void test_dm_qos_queue_stats_action_on_read(UNUSED void** state) {
    uint32_t index = 1;
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxd_object_t* queue_stats_instance = NULL;

    queue_stats_instance = get_queue_stats_instance(index);
    assert_non_null(queue_stats_instance);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_action_object_read(queue_stats_instance, NULL, action_object_read, &args, &retval, NULL), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);

}

void test_qos_queue_stats_new_delete(UNUSED void** state) {
    int retval = -1;
    qos_queue_stats_t* queue_stats = NULL;
    amxd_object_t* queue_stats_instance = NULL;

    queue_stats_instance = queue_stats_add_instance("test-queue-stats-1", true, NULL, "QoS.Queue.queue-1");
    assert_non_null(queue_stats_instance);

    retval = qos_queue_stats_new(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_queue_stats_new(&queue_stats, NULL);
    assert_int_equal(retval, -1);

    retval = qos_queue_stats_new(NULL, queue_stats_instance);
    assert_int_equal(retval, -1);

    retval = qos_queue_stats_new(&queue_stats, queue_stats_instance);
    assert_int_equal(retval, 0);
    assert_non_null(queue_stats);

    retval = qos_queue_stats_delete(NULL);
    assert_int_equal(retval, -1);

    retval = qos_queue_stats_delete(&queue_stats);
    assert_int_equal(retval, 0);
    assert_null(queue_stats);

    amxd_object_delete(&queue_stats_instance);
}

void test_qos_queue_stats_init_deinit(UNUSED void** state) {
    int retval = -1;
    qos_queue_stats_t queue_stats;
    amxd_object_t queue_stats_instance = {0};

    retval = qos_queue_stats_init(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_queue_stats_init(NULL, &queue_stats_instance);
    assert_int_equal(retval, -1);

    retval = qos_queue_stats_init(&queue_stats, NULL);
    assert_int_equal(retval, -1);

    retval = qos_queue_stats_init(&queue_stats, &queue_stats_instance);
    assert_int_equal(retval, 0);

    retval = qos_queue_stats_deinit(NULL);
    assert_int_equal(retval, -1);

    retval = qos_queue_stats_deinit(&queue_stats);
    assert_int_equal(retval, 0);
}

void test_qos_queue_stats_get_dm_object(UNUSED void** state) {
    int retval = -1;
    amxd_object_t* queue_stats_instance = NULL;
    amxd_object_t* dm_object = NULL;
    qos_queue_stats_t* queue_stats = NULL;

    queue_stats_instance = queue_stats_add_instance("test-queue-stats-2", true, NULL, "QoS.Queue.queue-2");
    assert_non_null(queue_stats_instance);

    retval = qos_queue_stats_new(&queue_stats, queue_stats_instance);
    assert_int_equal(retval, 0);

    dm_object = qos_queue_stats_get_dm_object(NULL);
    assert_null(dm_object);

    dm_object = qos_queue_stats_get_dm_object(queue_stats);
    assert_non_null(dm_object);
    assert_ptr_equal(queue_stats_instance, dm_object);
    assert_ptr_equal(dm_object->priv, queue_stats);

    retval = qos_queue_stats_delete(&queue_stats);
    assert_int_equal(retval, 0);

    amxd_object_delete(&queue_stats_instance);
}

void test_qos_queue_stats_dm_get_set_status(UNUSED void** state) {
    int retval = -1;
    uint32_t index = 3;
    amxd_object_t* queue_stats_instance = NULL;
    qos_queue_stats_t* queue_stats = NULL;
    qos_status_t status;
    qos_status_t valid_status = QOS_STATUS_DISABLED;

    queue_stats_instance = get_queue_stats_instance(index);
    assert_non_null(queue_stats_instance->priv);

    queue_stats = (qos_queue_stats_t*) queue_stats_instance->priv;
    assert_non_null(queue_stats);

    status = qos_queue_stats_dm_get_status(NULL);
    assert_int_equal(status, QOS_STATUS_DISABLED);

    //use a queuestats object with parameter enabled=false to test these functions
    status = qos_queue_stats_dm_get_status(queue_stats);
    assert_int_equal(status, QOS_STATUS_DISABLED);

    retval = qos_queue_stats_dm_set_status(queue_stats, QOS_STATUS_LAST);
    status = qos_queue_stats_dm_get_status(queue_stats);
    assert_int_equal(retval, -1);
    assert_int_equal(status, QOS_STATUS_DISABLED);

    while(valid_status < QOS_STATUS_LAST) {

        if(QOS_STATUS_ERROR_MISCONFIGURED == valid_status) {
            valid_status++;
            continue; //Not a valid status for a queuestats object.
        }

        retval = qos_queue_stats_dm_set_status(queue_stats, valid_status);
        status = qos_queue_stats_dm_get_status(queue_stats);
        assert_int_equal(retval, 0);
        assert_int_equal(status, valid_status);

        valid_status++;
    }
}

void test_qos_queue_stats_asked(UNUSED void** state) {
    amxd_object_t* obj_returned = NULL;
    amxd_object_t* object;
    qos_queue_t* queue;
    struct _qos_node* node;

    //first set the object to an instance without the parameter "Queue"
    object = get_queue_stats_instance(2);
    assert_non_null(object);

    queue = (qos_queue_t*) object->priv;
    assert_non_null(queue);
    node = queue->node;

    //return true on when_null catch.
    obj_returned = qos_queue_stats_asked(NULL);
    assert_null(obj_returned);

    //return true on when_null catch.
    queue->node = NULL;
    object->priv = queue;
    obj_returned = qos_queue_stats_asked(object);
    assert_ptr_equal(obj_returned, object);
    queue->node = node;

    //set object to an instance with the paramater Enable on false
    object = get_queue_stats_instance(3);
    assert_non_null(object->priv);
    obj_returned = qos_queue_stats_asked(object);
    assert_ptr_equal(obj_returned, object);

    //set the object to an instance with the parameter "Queue" that points to a queue, but __wrap_qos_module_exec_api_function fails
    object = get_queue_stats_instance(4);
    assert_non_null(object->priv);
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_RETRIEVE_STATS);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, -1);
    obj_returned = qos_queue_stats_asked(object);
    assert_ptr_equal(obj_returned, object);

    //__wrap_qos_module_exec_api_function succeeds
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_RETRIEVE_STATS);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, 0);
    obj_returned = qos_queue_stats_asked(object);
    assert_ptr_equal(obj_returned, object);
}

void test_qos_queue_stats_update_stats(UNUSED void** state) {
    uint32_t index = 3;
    uint32_t tx_packets;
    uint32_t tx_bytes;
    uint32_t dropped_packets;
    uint32_t dropped_bytes;
    amxd_object_t* queue_stats_instance = NULL;
    amxc_var_t stats;

    assert_int_equal(amxc_var_init(&stats), 0);

    amxc_var_set_type(&stats, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &stats, "TxPackets", 1234);
    amxc_var_add_key(uint32_t, &stats, "TxBytes", 12345);
    amxc_var_add_key(uint32_t, &stats, "DroppedPackets", 123);
    amxc_var_add_key(uint32_t, &stats, "DroppedBytes", 1234);

    queue_stats_instance = get_queue_stats_instance(index);
    assert_non_null(queue_stats_instance->priv);

    qos_queue_stats_update_stats(queue_stats_instance, &stats);

    tx_packets = amxd_object_get_value(uint32_t, queue_stats_instance, "OutputPackets", NULL);
    assert_int_equal(tx_packets, 1234);

    tx_bytes = amxd_object_get_value(uint32_t, queue_stats_instance, "OutputBytes", NULL);
    assert_int_equal(tx_bytes, 12345);

    dropped_packets = amxd_object_get_value(uint32_t, queue_stats_instance, "DroppedPackets", NULL);
    assert_int_equal(dropped_packets, 123);

    dropped_bytes = amxd_object_get_value(uint32_t, queue_stats_instance, "DroppedBytes", NULL);
    assert_int_equal(dropped_bytes, 1234);

    amxc_var_clean(&stats);
}