/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__QOS_H__)
#define __QOS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxm/amxm.h>

#include <qoscommon/qos-queue.h>
#include <qoscommon/qos-scheduler.h>
#include <qoscommon/qos-shaper.h>
#include <qoscommon/qos-queue-stats.h>
#include <qoscommon/qos-type.h>
#include <qosnode/qos-node-api.h>
#include "qos-classification.h"

#define QOS_TR181_DEVICE_QOS_PATH "QoS"
#define CONTROLLER "Controller"
#define MOD_QOS_CTRL "qos-ctrl"
#define PARAMS "Parameters"

typedef struct _qos {
    amxd_object_t* dm_object; /**< The data model object */
} qos_t;

int qos_classification_dm_set_status(const qos_classification_t* const classification, const qos_status_t status);
qos_status_t qos_classification_dm_get_status(const qos_classification_t* const classification);


int qos_new(qos_t** qos, amxd_object_t* const qos_instance);
int qos_delete(qos_t** qos);
int qos_init(qos_t* const qos, amxd_object_t* const qos_instance);
int qos_deinit(qos_t* const qos);

qos_t* qos_get_dm_object_priv_data(const amxd_object_t* const object);

amxd_object_t* qos_get_dm_object(const qos_t* const qos);

uint32_t qos_dm_get_max_classification_entries(const qos_t* const qos);
uint32_t qos_dm_get_max_queue_entries(const qos_t* const qos);

int qos_queue_activate(const qos_queue_t* const queue);
int qos_queue_deactivate(const qos_queue_t* const queue);
void qos_queue_interface_netdevname_changed(const char* sig_name, const amxc_var_t* data, void* priv);
int qos_shaper_activate(const qos_shaper_t* const shaper);
int qos_shaper_deactivate(const qos_shaper_t* const shaper);
void qos_shaper_interface_netdevname_changed(const char* sig_name, const amxc_var_t* data, void* priv);
int qos_scheduler_activate(const qos_scheduler_t* const scheduler);
int qos_scheduler_deactivate(const qos_scheduler_t* const scheduler);
void qos_scheduler_interface_netdevname_changed(const char* sig_name, const amxc_var_t* data, void* priv);
int qos_queue_stats_activate(const qos_queue_stats_t* const queue_stats);
int qos_queue_stats_deactivate(const qos_queue_stats_t* const queue_stats);
amxd_object_t* qos_queue_stats_asked(amxd_object_t* object);
void qos_queue_stats_update_stats(amxd_object_t* object, amxc_var_t* stats);
amxd_object_t* qos_queue_asked(amxd_object_t* object);
void qos_queue_update_stats(amxd_object_t* object, amxc_var_t* stats);
void qos_activate_children(qos_node_t* node);
void qos_deactivate_children(qos_node_t* node);

#ifdef __cplusplus
}
#endif

#endif // __QOS_H__
