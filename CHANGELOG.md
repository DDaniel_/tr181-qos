# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v3.3.5 - 2023-08-04(09:39:44 +0000)

### Changes

- Move odl configuration to board-configurator

## Release v3.3.4 - 2023-05-09(14:08:33 +0000)

### Other

- - [TR-181]gsdm is not working properly on Device.QoS.Classification

## Release v3.3.3 - 2023-03-15(14:56:46 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v3.3.2 - 2023-03-02(12:47:10 +0000)

### Changes

- - [QoS] Controller parameters must be protected

## Release v3.3.1 - 2023-02-24(15:42:30 +0000)

### Other

- [CI] Add missing dependency on libqosmodule

## Release v3.3.0 - 2023-02-06(15:44:14 +0000)

### Changes

- - [QOS] Add support for defaults.d directory in odl

## Release v3.2.0 - 2023-02-03(12:57:09 +0000)

### New

- - [libqosmodule] Integrate library

## Release v3.1.0 - 2023-01-25(09:54:50 +0000)

### Changes

- [tr181-qos] random: QoS.Queue reported as misconfigured

## Release v3.0.9 - 2023-01-10(11:45:52 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v3.0.8 - 2022-11-25(15:25:47 +0000)

### Fixes

- - [VLAN][QOS][WNC] Changing the WANMode to vlan, QoS queues creating will fail
- fail") added the netmodel flag 'eth_intf' to the

## Release v3.0.7 - 2022-11-23(09:45:19 +0000)

### Other

- - [tr181-qos] Update documentation

## Release v3.0.6 - 2022-11-02(09:58:36 +0000)

### Fixes

- - [VLAN][QOS][WNC] Changing the WANMode to vlan, QoS queues creating will fail

## Release v3.0.5 - 2022-10-10(09:38:40 +0000)

### Fixes

- - [QoS] Queues are in state Error if the wan cable is not connected

## Release v3.0.4 - 2022-10-10(08:58:35 +0000)

### Fixes

- - [QoS] Queues are in state Error if the wan cable is not connected

## Release v3.0.3 - 2022-09-22(16:13:59 +0000)

### Fixes

- - [tr181-qos] Do not require on NetModel.Intf

## Release v3.0.2 - 2022-09-14(06:55:54 +0000)

### Fixes

- - [QoS] Segmentation Error when trying to access QoS. datamodel

## Release v3.0.1 - 2022-09-12(14:14:25 +0000)

### Fixes

- Enable core dumps by default

## Release v3.0.0 - 2022-09-08(07:39:20 +0000)

### New

- - [tr181-qos] Integrate netmodel to translate interfaces

## Release v2.1.1 - 2022-09-05(12:59:15 +0000)

### New

- - [prpl][qos] Expand unit tests for node creation

## Release v2.1.0 - 2022-09-01(10:07:51 +0000)

### New

- - [prpl][qos] Integrate new TR-181 parameters for Device.QoS v2.16

## Release v2.0.1 - 2022-08-19(14:28:29 +0000)

## Release v2.0.0 - 2022-08-03(08:38:34 +0000)

## Release v1.1.11 - 2022-07-01(08:01:34 +0000)

### Fixes

- - [tr181-qos][Regression] Queue.TrafficClasses values unexpectedly overwritten

## Release v1.1.10 - 2022-05-02(09:32:55 +0000)

### Fixes

- - [tr181-qos] Set realistic shaping rates for the default configuration

## Release v1.1.9 - 2022-03-24(09:27:55 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v1.1.8 - 2022-02-25(09:04:07 +0000)

### Other

- Update documentation
- Update documentation

## Release v1.1.7 - 2022-02-24(09:22:01 +0000)

### Fixes

- - [prpl][qos] QueueKey must be used as queue identifier

## Release v1.1.6 - 2022-02-17(15:17:17 +0000)

### Changes

- - [tr181-qos] Rework variants for tc module

## Release v1.1.5 - 2022-02-08(11:18:22 +0000)

### Other

- - [tr181-qos] Document code
- - [tr181-qos] Document code

## Release v1.1.4 - 2022-02-04(10:07:00 +0000)

### Changes

- - [prpl][qos] Startup order does not work

## Release v1.1.3 - 2022-01-26(15:51:33 +0000)

### Changes

- - [tr181-qos] move classificationkey to queuekey and add interface parameter to scheduler

## Release v1.1.2 - 2022-01-21(08:51:02 +0000)

### Changes

- - [tr181-qos] Remove X_PRPL prefixes

## Release v1.1.1 - 2022-01-03(14:07:50 +0000)

### Changes

- license: change to BSD-2-Clause-Patent license

### Other

- - Use BSD-2-Clause-Patent license

## Release v1.1.0 - 2021-12-14(10:51:58 +0000)

### New

- queuestats: unit tests

### Other

- - Adding unit tests for queue stats

## Release v1.0.7 - 2021-12-13(11:14:44 +0000)

### Changes

- node: Adapt nodes default config and make children and parents not protected.
- nodes: Use mod_dmext

## Release v1.0.6 - 2021-12-10(14:22:08 +0000)

### Changes

- tr181-qos: Make controller and supportedControllers parameter visible in dm

## Release v1.0.5 - 2021-11-25(09:31:00 +0000)

### Other

- [ACL] Add default ACL file for the QoS Manager

## Release v1.0.4 - 2021-11-17(11:55:21 +0000)

### Other

- Opensource component

## Release v1.0.3 - 2021-11-16(10:18:27 +0000)

### Changes

- trace: do not print pointers in the log

### Other

- - [tr181-qos] Cleanup logging

## Release v1.0.2 - 2021-11-16(09:57:08 +0000)

### Changes

- odl: set log level to 200

### Other

- - [tr181-qos] Set default log level to 200

## Release v1.0.1 - 2021-11-10(12:57:37 +0000)

### Changes

- tr181-qos: queuestats for qos-core

## Release v1.0.0 - 2021-11-05(14:09:17 +0000)

### Breaking

- - [tr181-qos] Re-work variant to (de-)activate a queue

### New

- - [tr181-qos] Support for ordered classifications
- node: functions to query a list of parents or children by type

### Changes

- cleanup: remove X_PRPL_ references
- qos: remove unnecessary functions between controller and qos
- queue: proper return values

### Other

- Support for queue (de)activation
- - [tr181-qos] Remove X_PRPL_ references
- - Remove layer between controller and qos
- - [tr181-qos] functions to query a list of parents and children by type

## Release v0.2.0 - 2021-10-11(15:45:23 +0000)

### New

- node: add functions to find a parent or child node by type

### Other

- - [tr181-qos] Add functions to find a parent or child node by type

## Release v0.1.0 - 2021-10-11(14:13:45 +0000)

### New

- node: link nodes to lower layer objects

### Fixes

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components

### Other

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components
- [tr181-qos] Add public header directory
- [tr181-qos] Custom QoS.X_PRPL_Node object
- [Gitlab CI][Unit tests][valgrind] Pipelines don't stop when memory leaks are detected
- [tr181-qos] Custom QoS.X_PRPL_Scheduler object
- Remove X_PRPL in Filenames, Functions and Member vars
- - tr181-qos: Integrating separation of concerns
- - [tr181-qos] Replace data model functions to retrieve an object
- - [tr181-qos] Enable all node types
- - [tr181-qos] Link nodes to lower layer objects

## Release 0.0.2 - 2021-05-26(18:26:42 +0200)

- Fixed compile issue.

## Release 0.0.1 - 2021-05-07(14:47:37 +0200)

### Added

- dm-qos-queue.{h,c} - Event handeling for new, removed or changed Device.QoS.Queue.{i} instances.
- qos-queue.{h,c} - The internal representation of a Device.QoS.Queue.{i} instance.
- qos.{h,c} - The internal representation of a Device.QoS object.
- qos-controller.{h,c} - Initializes the structures qos_t, qos_queue_t.
- test directory - Test and coverage reports are added for qos.h and qos-queue.h


