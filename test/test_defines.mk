MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c)

WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. -I../mocks \
		  -fkeep-inline-functions -fkeep-static-functions \
		   -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread \
		  -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500

CFLAGS += -DUNIT_TEST

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lamxc -lamxm -lamxp -lamxd -lamxo -lqosmodule -ldl -lpthread -lfwrules -lfwinterface -lsahtrace -lqoscommon -lqosnode -lnetmodel

LDFLAGS += -g

MOCK_SRC_DIR := $(realpath ../mocks)
MOCK_SOURCES = $(wildcard $(MOCK_SRC_DIR)/*.c)

# libqosmodule wraps
MOCK_WRAP = qos_module_exec_api_function

LDFLAGS += $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
SOURCES += $(MOCK_SOURCES)