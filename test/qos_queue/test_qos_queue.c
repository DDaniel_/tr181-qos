/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include <qosmodule/api.h>

#include "qos-assert.h"
#include "dm_tr181-qos.h"
#include "dm-qos-queue.h"

#include "libqosmodule_mock.h"
#include "test_qos_queue.h"
#include "tr181-qos.h"
#include "qos.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "qos_queue_test.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static amxd_object_t* get_queue_instance(uint32_t index) {
    amxd_object_t* queue_object = NULL;
    amxd_object_t* queue_instance = NULL;

    queue_object = amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_QUEUE_PATH);
    assert_non_null(queue_object);

    queue_instance = amxd_object_get_instance(queue_object, NULL, index);
    assert_non_null(queue_instance);

    return queue_instance;
}

int test_qos_queue_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "qos_main",
                                            AMXO_FUNC(_qos_main)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_queue_evt_instance_added",
                                            AMXO_FUNC(_dm_qos_queue_evt_instance_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_queue_evt_instance_changed",
                                            AMXO_FUNC(_dm_qos_queue_evt_instance_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_queue_action_on_read",
                                            AMXO_FUNC(_dm_qos_queue_action_on_read)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_qos_queue_action_on_destroy",
                                            AMXO_FUNC(_dm_qos_queue_action_on_destroy)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _qos_main(0, &dm, &parser);
    _qos_app_start(NULL, NULL, NULL);
    handle_events();

    return 0;
}

int test_qos_queue_teardown(UNUSED void** state) {
    _qos_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_qos_queue_new_delete(UNUSED void** state) {
    int retval = -1;
    qos_queue_t* queue = NULL;
    amxd_object_t queue_instance = {0};

    retval = qos_queue_new(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_queue_new(&queue, NULL);
    assert_int_equal(retval, -1);

    retval = qos_queue_new(NULL, &queue_instance);
    assert_int_equal(retval, -1);

    retval = qos_queue_new(&queue, &queue_instance);
    assert_int_equal(retval, 0);
    assert_non_null(queue);

    retval = qos_queue_delete(NULL);
    assert_int_equal(retval, -1);

    retval = qos_queue_delete(&queue);
    assert_int_equal(retval, 0);
    assert_null(queue);
}

void test_qos_queue_init_deinit(UNUSED void** state) {
    int retval = -1;
    qos_queue_t queue;
    amxd_object_t queue_instance = {0};

    retval = qos_queue_init(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_queue_init(NULL, &queue_instance);
    assert_int_equal(retval, -1);

    retval = qos_queue_init(&queue, NULL);
    assert_int_equal(retval, -1);

    retval = qos_queue_init(&queue, &queue_instance);
    assert_int_equal(retval, 0);

    retval = qos_queue_deinit(NULL);
    assert_int_equal(retval, -1);

    retval = qos_queue_deinit(&queue);
    assert_int_equal(retval, 0);
}

void test_dm_qos_queue_evt_instance_added(UNUSED void** state) {
    amxd_object_t* queue_object = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;

    queue_object = amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_QUEUE_PATH);
    assert_non_null(queue_object);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(uint32_t, params, "BufferLength", 2048);
    amxc_var_add_key(uint32_t, params, "Precedence", 1);
    amxc_var_add_key(cstring_t, params, "SchedulerAlgorithm", "WRR");
    amxc_var_add_key(cstring_t, params, "Alias", "test_queue4");

    assert_int_equal(amxd_object_invoke_function(queue_object, "_add", &args, &ret), amxd_status_ok);
    assert_false(amxc_var_is_null(&ret));

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_dm_qos_queue_evt_instance_changed(UNUSED void** state) {
    uint32_t index = 4;
    amxc_var_t params;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;
    bool enable = false;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance);
    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    enable = qos_queue_dm_get_enable(queue);
    assert_true(enable);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &params, "Enable", false);
    amxd_object_send_changed(queue_instance, &params, false);
    amxc_var_clean(&params);

    handle_events();

}

void test_dm_qos_queue_action_on_read(UNUSED void** state) {
    uint32_t index = 1;
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxd_object_t* queue_instance = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_action_object_read(queue_instance, NULL, action_object_read, &args, &retval, NULL), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_dm_qos_queue_action_on_destroy(UNUSED void** state) {
    uint32_t index = 4;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance);
    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    amxd_object_delete(&queue_instance);
    assert_null(queue_instance);
}

void test_qos_queue_get_dm_object(UNUSED void** state) {
    uint32_t index = 1;
    amxd_object_t* queue_instance = NULL;
    amxd_object_t* dm_object = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    dm_object = qos_queue_get_dm_object(NULL);
    assert_null(dm_object);

    dm_object = qos_queue_get_dm_object(queue);
    assert_non_null(dm_object);
    assert_ptr_equal(queue_instance, dm_object);
}

void test_qos_queue_dm_get_enable(UNUSED void** state) {
    uint32_t index = 1;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;
    bool enable = true;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    enable = qos_queue_dm_get_enable(NULL);
    assert_false(enable);

    enable = qos_queue_dm_get_enable(queue);
    assert_true(enable);
}

void test_qos_queue_dm_set_enable(UNUSED void** state) {
    int retval = -1;
    uint32_t index = 1;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    retval = qos_queue_dm_set_enable(NULL, false);
    assert_int_equal(retval, -1);

    retval = qos_queue_dm_set_enable(NULL, true);
    assert_int_equal(retval, -1);

    retval = qos_queue_dm_set_enable(queue, false);
    assert_int_equal(retval, 0);
    assert_false(qos_queue_dm_get_enable(queue));

    retval = qos_queue_dm_set_enable(queue, true);
    assert_int_equal(retval, 0);
    assert_true(qos_queue_dm_get_enable(queue));
}

void test_qos_queue_dm_get_set_status(UNUSED void** state) {
    int retval = -1;
    uint32_t index = 1;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;
    qos_status_t status = QOS_STATUS_DISABLED;
    qos_status_t valid_status = QOS_STATUS_DISABLED;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    status = qos_queue_dm_get_status(NULL);
    assert_int_equal(status, QOS_STATUS_DISABLED);

    //first set status to enabled because core is tested without modules installed
    retval = qos_queue_dm_set_status(queue, QOS_STATUS_ENABLED);
    assert_int_equal(retval, 0);
    status = qos_queue_dm_get_status(queue);
    assert_int_equal(status, QOS_STATUS_ENABLED);

    retval = qos_queue_dm_set_status(queue, QOS_STATUS_LAST);
    status = qos_queue_dm_get_status(queue);
    assert_int_equal(retval, -1);
    assert_int_equal(status, QOS_STATUS_ENABLED);

    while(valid_status < QOS_STATUS_LAST) {
        retval = qos_queue_dm_set_status(queue, valid_status);
        status = qos_queue_dm_get_status(queue);
        assert_int_equal(retval, 0);
        assert_int_equal(status, valid_status);

        valid_status++;
    }

}

void test_qos_queue_dm_get_alias(UNUSED void** state) {
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;
    const char* alias = NULL;

    alias = qos_queue_dm_get_alias(NULL);
    assert_null(alias);

    queue_instance = get_queue_instance(1);
    assert_non_null(queue_instance);
    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);
    alias = qos_queue_dm_get_alias(queue);
    assert_non_null(alias);
    assert_string_equal(alias, "test_queue_default");

    queue_instance = get_queue_instance(2);
    assert_non_null(queue_instance);
    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);
    alias = qos_queue_dm_get_alias(queue);
    assert_non_null(alias);
    assert_string_equal(alias, "test_queue2");

    queue_instance = get_queue_instance(3);
    assert_non_null(queue_instance);
    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);
    alias = qos_queue_dm_get_alias(queue);
    assert_non_null(alias);
    assert_string_equal(alias, "test_queue3");
}

void test_qos_queue_dm_get_set_drop_algorithm(UNUSED void** state) {
    int retval = -1;
    uint32_t index = 1;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;
    qos_queue_drop_algorithm_t algorithm = QOS_QUEUE_DROP_ALGORITHM_RED;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    algorithm = qos_queue_dm_get_drop_algorithm(NULL);
    assert_int_equal(algorithm, QOS_QUEUE_DROP_ALGORITHM_RED);

    algorithm = qos_queue_dm_get_drop_algorithm(queue);
    assert_int_equal(algorithm, QOS_QUEUE_DROP_ALGORITHM_DT);

    retval = qos_queue_dm_set_drop_algorithm(queue, QOS_QUEUE_DROP_ALGORITHM_LAST);
    algorithm = qos_queue_dm_get_drop_algorithm(queue);
    assert_int_equal(retval, -1);
    assert_int_equal(algorithm, QOS_QUEUE_DROP_ALGORITHM_DT);

    retval = qos_queue_dm_set_drop_algorithm(queue, QOS_QUEUE_DROP_ALGORITHM_RED);
    algorithm = qos_queue_dm_get_drop_algorithm(queue);
    assert_int_equal(retval, 0);
    assert_int_equal(algorithm, QOS_QUEUE_DROP_ALGORITHM_RED);

    retval = qos_queue_dm_set_drop_algorithm(queue, QOS_QUEUE_DROP_ALGORITHM_DT);
    algorithm = qos_queue_dm_get_drop_algorithm(queue);
    assert_int_equal(retval, 0);
    assert_int_equal(algorithm, QOS_QUEUE_DROP_ALGORITHM_DT);

    retval = qos_queue_dm_set_drop_algorithm(queue, QOS_QUEUE_DROP_ALGORITHM_WRED);
    algorithm = qos_queue_dm_get_drop_algorithm(queue);
    assert_int_equal(retval, 0);
    assert_int_equal(algorithm, QOS_QUEUE_DROP_ALGORITHM_WRED);

    retval = qos_queue_dm_set_drop_algorithm(queue, QOS_QUEUE_DROP_ALGORITHM_BLUE);
    algorithm = qos_queue_dm_get_drop_algorithm(queue);
    assert_int_equal(retval, 0);
    assert_int_equal(algorithm, QOS_QUEUE_DROP_ALGORITHM_BLUE);

}

void test_qos_queue_dm_get_precedence(UNUSED void** state) {
    uint32_t index = 1;

    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;
    uint32_t precedence;
    uint32_t precedence_dm = 3;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    precedence = qos_queue_dm_get_precedence(NULL);
    assert_int_equal(precedence, 1);

    precedence = qos_queue_dm_get_precedence(queue);
    assert_int_equal(precedence, precedence_dm);
}

void test_qos_queue_dm_get_traffic_classes(UNUSED void** state) {
    uint32_t index = 1;
    const char* traffic_classes = NULL;
    const char* traffic_classes_dm = "1, 2, 3";
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    traffic_classes = qos_queue_dm_get_traffic_classes(NULL);
    assert_null(traffic_classes);

    traffic_classes = qos_queue_dm_get_traffic_classes(queue);
    assert_non_null(traffic_classes);
    assert_string_equal(traffic_classes, traffic_classes_dm);
}

void test_qos_queue_dm_get_all_interfaces(UNUSED void** state) {
    uint32_t index = 1;
    bool all_interfaces;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    all_interfaces = qos_queue_dm_get_all_interfaces(NULL);
    assert_false(all_interfaces);

    all_interfaces = qos_queue_dm_get_all_interfaces(queue);
    assert_true(all_interfaces);

}

void test_qos_queue_dm_get_hardware_assisted(UNUSED void** state) {
    uint32_t index = 1;
    bool hw_assisted;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    hw_assisted = qos_queue_dm_get_hardware_assisted(NULL);
    assert_false(hw_assisted);

    hw_assisted = qos_queue_dm_get_hardware_assisted(queue);
    assert_true(hw_assisted);
}

void test_qos_queue_dm_get_buffer_length(UNUSED void** state) {
    uint32_t index = 1;
    uint32_t buffer;
    uint32_t buffer_dm = 128;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    buffer = qos_queue_dm_get_buffer_length(NULL);
    assert_int_equal(buffer, 0);

    buffer = qos_queue_dm_get_buffer_length(queue);
    assert_int_equal(buffer, buffer_dm);
}

void test_qos_queue_dm_get_weight(UNUSED void** state) {
    uint32_t index = 1;
    uint32_t weight;
    uint32_t weight_dm = 2;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    weight = qos_queue_dm_get_weight(NULL);
    assert_int_equal(weight, 0);

    weight = qos_queue_dm_get_weight(queue);
    assert_int_equal(weight, weight_dm);
}

void test_qos_queue_dm_get_red_threshold(UNUSED void** state) {
    uint32_t index = 1;
    uint32_t threshold;
    uint32_t threshold_dm = 5;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    threshold = qos_queue_dm_get_red_threshold(NULL);
    assert_int_equal(threshold, 0);

    threshold = qos_queue_dm_get_red_threshold(queue);
    assert_int_equal(threshold, threshold_dm);
}

void test_qos_queue_dm_get_red_percentage(UNUSED void** state) {
    uint32_t index = 1;
    uint32_t percentage;
    uint32_t percentage_dm = 10;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    percentage = qos_queue_dm_get_red_percentage(NULL);
    assert_int_equal(percentage, 0);

    percentage = qos_queue_dm_get_red_percentage(queue);
    assert_int_equal(percentage, percentage_dm);
}

void test_qos_queue_dm_get_scheduler_algorithm(UNUSED void** state) {
    uint32_t index = 1;
    qos_scheduler_algorithm_t scheduler;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    scheduler = qos_queue_dm_get_scheduler_algorithm(NULL);
    assert_int_equal(scheduler, QOS_SCHEDULER_ALGORITHM_SP);

    scheduler = qos_queue_dm_get_scheduler_algorithm(queue);
    assert_int_equal(scheduler, QOS_SCHEDULER_ALGORITHM_WFQ);
}

void test_qos_queue_dm_get_shaping_rate(UNUSED void** state) {
    uint32_t index = 1;
    int32_t rate;
    int32_t rate_dm = 20;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    rate = qos_queue_dm_get_shaping_rate(NULL);
    assert_int_equal(rate, -1);

    rate = qos_queue_dm_get_shaping_rate(queue);
    assert_int_equal(rate, rate_dm);
}

void test_qos_queue_dm_get_assured_rate(UNUSED void** state) {
    uint32_t index = 1;
    int32_t rate;
    int32_t rate_dm = 20;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    rate = qos_queue_dm_get_assured_rate(NULL);
    assert_int_equal(rate, -1);

    rate = qos_queue_dm_get_assured_rate(queue);
    assert_int_equal(rate, rate_dm);
}

void test_qos_queue_dm_get_shaping_burst_size(UNUSED void** state) {
    uint32_t index = 1;
    int32_t size;
    int32_t size_dm = 128;
    amxd_object_t* queue_instance = NULL;
    qos_queue_t* queue = NULL;

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    size = qos_queue_dm_get_shaping_burst_size(NULL);
    assert_int_equal(size, 0);

    size = qos_queue_dm_get_shaping_burst_size(queue);
    assert_int_equal(size, size_dm);
}

void test_qos_queue_update_stats(UNUSED void** state) {
    uint32_t index = 1;
    int32_t current_shaping_rate;
    int32_t current_assured_rate;
    amxd_object_t* queue_instance = NULL;
    amxc_var_t stats;

    assert_int_equal(amxc_var_init(&stats), 0);

    amxc_var_set_type(&stats, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(int32_t, &stats, "CurrentShapingRate", 70000);
    amxc_var_add_key(int32_t, &stats, "CurrentAssuredRate", 50000);

    queue_instance = get_queue_instance(index);
    assert_non_null(queue_instance->priv);

    qos_queue_update_stats(queue_instance, &stats);

    current_shaping_rate = amxd_object_get_value(int32_t, queue_instance, "CurrentShapingRate", NULL);
    assert_int_equal(current_shaping_rate, 70000);

    current_assured_rate = amxd_object_get_value(int32_t, queue_instance, "CurrentAssuredRate", NULL);
    assert_int_equal(current_assured_rate, 50000);

    amxc_var_clean(&stats);
}

void test_qos_queue_interface_netdevname_changed(UNUSED void** state) {
    amxc_var_t data;
    qos_queue_t* queue;
    amxd_object_t* queue_instance;

    assert_int_equal(amxc_var_init(&data), 0);
    assert_int_equal(amxc_var_set_type(&data, AMXC_VAR_ID_CSTRING), 0);

    queue_instance = get_queue_instance(1);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    expect_any_always(__wrap_qos_module_exec_api_function, id);
    expect_any_always(__wrap_qos_module_exec_api_function, module_name);
    will_return_always(__wrap_qos_module_exec_api_function, 0);

    qos_queue_interface_netdevname_changed(NULL, NULL, NULL);
    qos_queue_interface_netdevname_changed(NULL, NULL, queue);

    assert_int_equal(amxc_var_set_cstring_t(&data, ""), 0);
    qos_queue_interface_netdevname_changed(NULL, &data, NULL);
    qos_queue_interface_netdevname_changed(NULL, &data, queue);

    assert_string_equal(queue->intf_name, "");

    assert_int_equal(amxc_var_set_cstring_t(&data, "eth0"), 0);
    qos_queue_interface_netdevname_changed(NULL, &data, queue);

    assert_string_equal(queue->intf_name, "eth0");

    queue_instance = get_queue_instance(2);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);

    qos_queue_interface_netdevname_changed(NULL, NULL, NULL);
    qos_queue_interface_netdevname_changed(NULL, NULL, queue);

    assert_int_equal(amxc_var_set_cstring_t(&data, ""), 0);
    qos_queue_interface_netdevname_changed(NULL, &data, NULL);

    qos_queue_interface_netdevname_changed(NULL, &data, queue);
    assert_string_equal(queue->intf_name, "");

    assert_int_equal(amxc_var_set_cstring_t(&data, "eth0"), 0);
    qos_queue_interface_netdevname_changed(NULL, &data, queue);

    assert_string_equal(queue->intf_name, "eth0");

    amxc_var_clean(&data);
}

void test_qos_queue_activate(UNUSED void** state) {
    int retval = -1;
    qos_queue_t* queue;
    amxd_object_t* queue_instance;
    struct _qos_node* node;

    queue_instance = get_queue_instance(1);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);
    node = queue->node;

    // return true on when_null catch
    retval = qos_queue_activate(NULL);
    assert_int_equal(retval, -1);

    // return true on when_null catch.
    queue->node = NULL;
    retval = qos_queue_activate(queue);
    assert_int_equal(retval, -1);

    //__wrap_qos_module_exec_api_function fails.
    queue->node = node;
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_ACTIVATE_QUEUE);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, -1);
    retval = qos_queue_activate(queue);
    assert_int_equal(retval, -1);

    //__wrap_qos_module_exec_api_function fails but gives a misconfigured error message. In this case 11
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_ACTIVATE_QUEUE);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, 11);
    retval = qos_queue_activate(queue);
    assert_int_equal(retval, -1);

    //__wrap_qos_module_exec_api_function succeeds
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_ACTIVATE_QUEUE);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, 0);
    retval = qos_queue_activate(queue);
    assert_int_equal(retval, 0);
}

void test_qos_queue_deactivate(UNUSED void** state) {
    int retval = -1;
    qos_queue_t* queue;
    amxd_object_t* queue_instance;
    struct _qos_node* node;

    queue_instance = get_queue_instance(1);
    assert_non_null(queue_instance->priv);

    queue = (qos_queue_t*) queue_instance->priv;
    assert_non_null(queue);
    node = queue->node;

    // return true on when_null catch.
    retval = qos_queue_deactivate(NULL);
    assert_int_equal(retval, -1);

    // return true on when_null catch.
    queue->node = NULL;
    retval = qos_queue_deactivate(queue);
    assert_int_equal(retval, -1);

    //__wrap_qos_module_exec_api_function fails.
    queue->node = node;
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_DEACTIVATE_QUEUE);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, -1);
    retval = qos_queue_deactivate(queue);
    assert_int_equal(retval, -1);

    //__wrap_qos_module_exec_api_function fails but gives a misconfigured error message. In this case 11
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_DEACTIVATE_QUEUE);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, 11);
    retval = qos_queue_deactivate(queue);
    assert_int_equal(retval, -1);

    //__wrap_qos_module_exec_api_function succeeds
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_DEACTIVATE_QUEUE);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, 0);
    retval = qos_queue_deactivate(queue);
    assert_int_equal(retval, 0);

}

void test_qos_queue_asked(UNUSED void** state) {
    amxd_object_t* retval = NULL;
    qos_queue_t* queue;
    amxd_object_t* object;
    struct _qos_node* node;

    object = get_queue_instance(1);
    assert_non_null(object->priv);

    queue = (qos_queue_t*) object->priv;
    assert_non_null(queue);
    node = queue->node;

    // return true on when_null catch.
    retval = qos_queue_asked(NULL);
    assert_null(retval);

    // return true on when_null catch.
    object->priv = NULL;
    retval = qos_queue_asked(object);
    assert_ptr_equal(retval, object);

    //return true on when_null catch.
    queue->node = NULL;
    object->priv = queue;
    retval = qos_queue_asked(object);
    assert_ptr_equal(retval, object);
    queue->node = node;

    //set queue enabled on false
    object = get_queue_instance(3);
    retval = qos_queue_asked(object);
    assert_ptr_equal(retval, object);

    //set queue enabled on true but __wrap_qos_module_exec_api_function fails
    object = get_queue_instance(1);
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_RETRIEVE_QUEUE_STATS);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, -1);
    retval = qos_queue_asked(object);
    assert_ptr_equal(retval, object);

    //__wrap_qos_module_exec_api_function gives a misconfigured error message. In this case 11. Does the same as the test above.
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_RETRIEVE_QUEUE_STATS);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, 11);
    retval = qos_queue_asked(object);
    assert_ptr_equal(retval, object);

    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_RETRIEVE_QUEUE_STATS);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, 0);
    retval = qos_queue_asked(object);
    assert_ptr_equal(retval, object);
}